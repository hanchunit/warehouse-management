package com.gwlc.inventory.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gwlc.inventory.domain.dto.WarehouseDTO;
import com.gwlc.inventory.domain.entity.WarehouseDO;
import com.gwlc.inventory.domain.page.WarehousePage;
import com.gwlc.inventory.domain.query.WarehouseQuery;
import com.gwlc.inventory.domain.vo.SaveWarehouseCommodityVO;
import com.gwlc.inventory.domain.vo.SaveWarehouseVO;
import com.gwlc.inventory.mapper.WarehouseMapper;
import com.gwlc.inventory.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * @author hanchun
 */
@Service
public class WarehouseServiceImpl extends ServiceImpl<WarehouseMapper, WarehouseDO>
        implements WarehouseService {
    @Autowired
    WarehouseMapper warehouseMapper;

    /**
     * 仓库列表
     *
     * @param warehouse 仓库信息
     * @return 结果
     */
    @Override
    public List<WarehouseDTO> listWarehouses(WarehouseQuery warehouse) {
        return warehouseMapper.listWarehouses(warehouse);
    }

    /**
     * 分页
     *
     * @param warehouse 商品
     * @return 结果
     */
    @Override
    public WarehousePage pageWarehouse(WarehousePage warehouse) {
        warehouse.setCount(warehouseMapper.countVagueByWarehouse(warehouse.getQuery()));
        if (warehouse.getCount() > 0) {
            warehouse.setData(warehouseMapper.page(warehouse));
        }
        return warehouse;
    }

    /**
     * 根据标识获取仓库信息
     *
     * @param warehouseId 仓库标识
     * @return 结果
     */
    @Override
    public WarehouseDTO getWarehouse(Serializable warehouseId) {
        return warehouseMapper.getWarehouseById(warehouseId);
    }

    /**
     * 保存仓库信息
     *
     * @param warehouse 仓库信息
     * @return 结果
     */
    @Override
    public Boolean saveWarehouse(SaveWarehouseVO warehouse) {
        return warehouseMapper.saveWarehouse(warehouse) > 0;
    }

    /**
     * 更新仓库信息
     *
     * @param warehouse 仓库
     * @return 结果
     */
    @Override
    public Boolean updateWarehouse(SaveWarehouseVO warehouse) {
        return warehouseMapper.updateWarehouse(warehouse) > 0;
    }

    /**
     * 根据标识删除仓库
     *
     * @param warehouseId 仓库标识
     * @return 结果
     */
    @Override
    public Boolean deleteWarehouse(Long warehouseId) {
        return warehouseMapper.deleteWarehouse(warehouseId) > 0;
    }

    /**
     * 统计仓库数量
     *
     * @param warehouse 仓库信息
     * @return 结果
     */
    @Override
    public Integer countWarehouse(WarehouseQuery warehouse) {
        return warehouseMapper.countByWarehouse(warehouse);
    }

    /**
     * 保存仓库商品信息
     *
     * @param saveWarehouseCommodity 仓库商品信息
     * @return 结果
     */
    @Override
    public Boolean saveWarehouseCommodities(SaveWarehouseCommodityVO saveWarehouseCommodity) {
        return warehouseMapper.saveWarehouseCommodities(saveWarehouseCommodity) > 0;
    }
}




