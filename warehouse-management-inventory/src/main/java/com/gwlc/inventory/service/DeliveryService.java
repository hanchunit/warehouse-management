package com.gwlc.inventory.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gwlc.inventory.domain.dto.DeliveryDTO;
import com.gwlc.inventory.domain.entity.DeliveryDO;
import com.gwlc.inventory.domain.page.DeliveryPage;
import com.gwlc.inventory.domain.query.DeliveryQuery;
import com.gwlc.inventory.domain.vo.SaveDeliveryVO;

import java.io.Serializable;
import java.util.List;

/**
 * @author hanchun
 */
public interface DeliveryService extends IService<DeliveryDO> {

    /**
     * 模糊查询出库单列表
     *
     * @param delivery 出库单信息
     * @return 结果
     */
    List<DeliveryDTO> listDeliveries(DeliveryQuery delivery);

    /**
     * 分页
     *
     * @param delivery 入库
     * @return 结果
     */
    DeliveryPage pageDelivery(DeliveryPage delivery);

    /**
     * 精确查询出库单
     *
     * @param deliveryId 出库单标识
     * @return 结果
     */
    DeliveryDTO getDelivery(Serializable deliveryId);

    /**
     * 保存出库单
     *
     * @param saveDeliveryDetails 出库单
     * @return 结果
     */
    Boolean saveDeliveryService(SaveDeliveryVO saveDeliveryDetails);

    /**
     * 更新出库单信息
     *
     * @param delivery 出库单
     * @return 结果
     */
    Boolean updateDelivery(SaveDeliveryVO delivery);


    /**
     * 根据标识删除出库单
     *
     * @param deliveryId 出库单标识
     * @return 结果
     */
    Boolean deleteDelivery(Long deliveryId);
}
