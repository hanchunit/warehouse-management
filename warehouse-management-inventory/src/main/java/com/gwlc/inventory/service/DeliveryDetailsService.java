package com.gwlc.inventory.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gwlc.inventory.domain.entity.DeliveryDetailsDO;

/**
 *
 */
public interface DeliveryDetailsService extends IService<DeliveryDetailsDO> {

}
