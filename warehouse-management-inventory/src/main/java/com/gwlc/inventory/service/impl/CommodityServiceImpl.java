package com.gwlc.inventory.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gwlc.inventory.domain.dto.CommodityDTO;
import com.gwlc.inventory.domain.entity.CommodityClassifyDO;
import com.gwlc.inventory.domain.entity.CommodityDO;
import com.gwlc.inventory.domain.page.CommodityPage;
import com.gwlc.inventory.domain.query.CommodityQuery;
import com.gwlc.inventory.domain.vo.EditCommodityVO;
import com.gwlc.inventory.domain.vo.SaveCommodityVO;
import com.gwlc.inventory.mapper.CommodityMapper;
import com.gwlc.inventory.service.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author hanchun
 */
@Service
public class CommodityServiceImpl extends ServiceImpl<CommodityMapper, CommodityDO>
        implements CommodityService {
    @Autowired
    CommodityMapper commodityMapper;

    /**
     * 商品列表
     *
     * @param commodity 商品查询条件
     * @return 商品列表
     */
    @Override
    public List<CommodityDTO> listCommodities(CommodityQuery commodity) {
        return commodityMapper.listCommodities(commodity);
    }

    /**
     * 分页
     *
     * @param commodity 商品
     * @return 结果
     */
    @Override
    public CommodityPage pageCommodities(CommodityPage commodity) {
        commodity.setCount(commodityMapper.countVagueByCommodity(commodity.getQuery()));
        if (commodity.getCount() > 0) {
            commodity.setData(commodityMapper.page(commodity));
        }
        return commodity;
    }

    /**
     * 获取商品
     *
     * @param commodityId 商品标识
     * @return 商品
     */
    @Override
    public CommodityDTO getCommodity(Serializable commodityId) {
        return commodityMapper.getCommodityById(commodityId);
    }

    /**
     * 保存商品
     *
     * @param commodity 商品
     * @return 结果
     */
    @Override

    public Boolean saveCommodity(SaveCommodityVO commodity) {

        if (commodityMapper.saveCommodity(commodity) <= 0) {
            return false;
        }
        if (commodity.getClassifies() == null || commodity.getClassifies().isEmpty()) {
            return true;
        }

        List<CommodityClassifyDO> commodityClassifies = new ArrayList<>();
        for (Long classify : commodity.getClassifies()) {
            commodityClassifies.add(new CommodityClassifyDO(commodity.getCommodityId(), classify));
        }

        return commodityMapper.saveCommodityClassify(commodityClassifies) > 0;


    }

    /**
     * 更新商品
     *
     * @param commodity 商品
     * @return 结果
     */
    @Override
    public Boolean updateCommodity(EditCommodityVO commodity) {


        if (commodityMapper.updateCommodity(commodity) <= 0) {
            return false;
        }

        List<CommodityClassifyDO> saveCommodityClassifies = new ArrayList<>();
        List<CommodityClassifyDO> deleteCommodityClassifies = new ArrayList<>();
        if (commodity.getSaveClassifies() != null && !commodity.getSaveClassifies().isEmpty()) {
            for (Long classify : commodity.getSaveClassifies()) {
                saveCommodityClassifies.add(new CommodityClassifyDO(commodity.getCommodityId(), classify));
            }
        }
        if (commodity.getDeleteClassifies() != null && !commodity.getSaveClassifies().isEmpty()) {
            for (Long classify : commodity.getDeleteClassifies()) {
                deleteCommodityClassifies.add(new CommodityClassifyDO(commodity.getCommodityId(), classify));
            }
        }
        //判断保存分类是否为空
        if (saveCommodityClassifies.isEmpty()) {
            //判断删除分类是否为空
            if (deleteCommodityClassifies.isEmpty()) {
                //全空返回true
                return true;
            } else {
                //删除分类不为空返回删除分类结果
                return commodityMapper.deleteCommodityClassify(deleteCommodityClassifies) > 0;
            }
        } else {
            //判断删除分类是否为空
            if (deleteCommodityClassifies.isEmpty()) {

                //删除分类为空返回保存分类结果
                return commodityMapper.saveCommodityClassify(saveCommodityClassifies) > 0;
            } else {

                //删除分类不为空返回删除分类结果和保存分类结果
                return commodityMapper.deleteCommodityClassify(deleteCommodityClassifies) > 0 && commodityMapper.saveCommodityClassify(saveCommodityClassifies) > 0;
            }
        }

    }

    /**
     * 删除商品及商品分类信息
     *
     * @param commodityId 商品标识
     * @return 结果
     */
    @Override
    public Boolean deleteCommodity(Long commodityId) {

        if (commodityMapper.deleteCommodityById(commodityId) <= 0) {
            return false;
        }
        int classifyCount = commodityMapper.countByCommodityClassify(new CommodityClassifyDO(commodityId, null));
        if (classifyCount <= 0) {
            return true;
        }


        return commodityMapper.deleteCommodityClassifyById(commodityId) == classifyCount;
    }

    /**
     * 删除商品及商品分类信息
     *
     * @param commodity 商品
     * @return 结果
     */
    @Override
    public Integer countCommodity(CommodityQuery commodity) {
        return commodityMapper.countByCommodity(commodity);
    }
}




