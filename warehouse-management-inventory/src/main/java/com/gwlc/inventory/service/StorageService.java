package com.gwlc.inventory.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gwlc.inventory.domain.dto.StorageDTO;
import com.gwlc.inventory.domain.entity.StorageDO;
import com.gwlc.inventory.domain.query.StorageQuery;
import com.gwlc.inventory.domain.vo.SaveStorageVO;

import java.io.Serializable;
import java.util.List;

/**
 * @author hanchun
 */
public interface StorageService extends IService<StorageDO> {

    /**
     * 模糊查询入库单列表
     *
     * @param storage 入库单信息
     * @return 结果
     */
    List<StorageDTO> listStorages(StorageQuery storage);

    /**
     * 精确查询入库单
     *
     * @param storageId 入库单标识
     * @return 结果
     */
    StorageDTO getStorage(Serializable storageId);

    /**
     * 保存入库单
     *
     * @param saveStorageDetails 入库单
     * @return 结果
     */
    Boolean saveStorageService(SaveStorageVO saveStorageDetails);


}
