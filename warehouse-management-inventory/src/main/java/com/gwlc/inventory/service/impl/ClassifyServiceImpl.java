package com.gwlc.inventory.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gwlc.inventory.domain.entity.ClassifyDO;
import com.gwlc.inventory.mapper.ClassifyMapper;
import com.gwlc.inventory.service.ClassifyService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class ClassifyServiceImpl extends ServiceImpl<ClassifyMapper, ClassifyDO>
        implements ClassifyService {

}




