package com.gwlc.inventory.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gwlc.inventory.domain.entity.DeliveryDetailsDO;
import com.gwlc.inventory.mapper.DeliveryDetailsMapper;
import com.gwlc.inventory.service.DeliveryDetailsService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class DeliveryDetailsServiceImpl extends ServiceImpl<DeliveryDetailsMapper, DeliveryDetailsDO>
        implements DeliveryDetailsService {

}




