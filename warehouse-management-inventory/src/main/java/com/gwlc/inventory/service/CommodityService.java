package com.gwlc.inventory.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gwlc.inventory.domain.dto.CommodityDTO;
import com.gwlc.inventory.domain.entity.CommodityDO;
import com.gwlc.inventory.domain.page.CommodityPage;
import com.gwlc.inventory.domain.query.CommodityQuery;
import com.gwlc.inventory.domain.vo.EditCommodityVO;
import com.gwlc.inventory.domain.vo.SaveCommodityVO;

import java.io.Serializable;
import java.util.List;

/**
 * @author hanchun
 */
public interface CommodityService extends IService<CommodityDO> {
    /**
     * 商品列表
     *
     * @param commodity 商品查询条件
     * @return 商品列表
     */
    List<CommodityDTO> listCommodities(CommodityQuery commodity);

    /**
     * 分页
     *
     * @param commodity 商品
     * @return 结果
     */
    CommodityPage pageCommodities(CommodityPage commodity);

    /**
     * 获取商品
     *
     * @param commodityId 商品标识
     * @return 商品
     */
    CommodityDTO getCommodity(Serializable commodityId);

    /**
     * 保存商品
     *
     * @param commodity 商品
     * @return 结果
     */
    Boolean saveCommodity(SaveCommodityVO commodity);

    /**
     * 更新商品
     *
     * @param commodity 商品
     * @return 结果
     */
    Boolean updateCommodity(EditCommodityVO commodity);

    /**
     * 删除商品及商品分类信息
     *
     * @param commodityId 商品标识
     * @return 结果
     */
    Boolean deleteCommodity(Long commodityId);

    /**
     * 删除商品及商品分类信息
     *
     * @param commodity 商品
     * @return 结果
     */
    Integer countCommodity(CommodityQuery commodity);
}
