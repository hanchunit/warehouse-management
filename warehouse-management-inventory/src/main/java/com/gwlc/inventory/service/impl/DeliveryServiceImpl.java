package com.gwlc.inventory.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gwlc.inventory.domain.dto.DeliveryDTO;
import com.gwlc.inventory.domain.entity.DeliveryDO;
import com.gwlc.inventory.domain.page.DeliveryPage;
import com.gwlc.inventory.domain.query.DeliveryQuery;
import com.gwlc.inventory.domain.vo.SaveDeliveryVO;
import com.gwlc.inventory.mapper.DeliveryMapper;
import com.gwlc.inventory.service.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * @author hanchun
 */
@Service
public class DeliveryServiceImpl extends ServiceImpl<DeliveryMapper, DeliveryDO>
        implements DeliveryService {
    @Autowired
    DeliveryMapper deliveryMapper;

    /**
     * 模糊查询出库单列表
     *
     * @param delivery 出库单信息
     * @return 结果
     */
    @Override
    public List<DeliveryDTO> listDeliveries(DeliveryQuery delivery) {
        return deliveryMapper.listDeliveries(delivery);
    }

    /**
     * 分页
     *
     * @param delivery 商品
     * @return 结果
     */
    @Override
    public DeliveryPage pageDelivery(DeliveryPage delivery) {
        delivery.setCount(deliveryMapper.countVagueByDelivery(delivery.getQuery()));
        if (delivery.getCount() > 0) {
            delivery.setData(deliveryMapper.page(delivery));
        }
        return delivery;
    }


    /**
     * 精确查询出库单
     *
     * @param deliveryId 出库单标识
     * @return 结果
     */
    @Override
    public DeliveryDTO getDelivery(Serializable deliveryId) {
        return deliveryMapper.getDelivery(deliveryId);
    }

    /**
     * 保存出库单
     *
     * @param saveDeliveryDetails 出库单
     * @return 结果
     */
    @Override
    public Boolean saveDeliveryService(SaveDeliveryVO saveDeliveryDetails) {

        if (deliveryMapper.saveDelivery(saveDeliveryDetails) <= 0) {
            return false;
        }
        if (saveDeliveryDetails.getDeliveryDetails() == null || saveDeliveryDetails.getDeliveryDetails().isEmpty()) {
            return true;
        }

        return deliveryMapper.saveDeliveryDetails(saveDeliveryDetails) > 0;
    }

    /**
     * 更新出库单信息
     *
     * @param delivery 出库单
     * @return 结果
     */
    @Override
    public Boolean updateDelivery(SaveDeliveryVO delivery) {
        return deliveryMapper.updateDelivery(delivery) > 0;
    }

    /**
     * 根据标识删除出库单
     *
     * @param deliveryId 出库单标识
     * @return 结果
     */
    @Override
    public Boolean deleteDelivery(Long deliveryId) {
        return deliveryMapper.deleteDelivery(deliveryId) > 0;
    }


}




