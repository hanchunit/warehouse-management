package com.gwlc.inventory.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gwlc.inventory.domain.entity.StorageDetailsDO;

/**
 *
 */
public interface StorageDetailsService extends IService<StorageDetailsDO> {

}
