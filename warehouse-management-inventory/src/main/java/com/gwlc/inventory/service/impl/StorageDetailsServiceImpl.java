package com.gwlc.inventory.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gwlc.inventory.domain.entity.StorageDetailsDO;
import com.gwlc.inventory.mapper.StorageDetailsMapper;
import com.gwlc.inventory.service.StorageDetailsService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class StorageDetailsServiceImpl extends ServiceImpl<StorageDetailsMapper, StorageDetailsDO>
        implements StorageDetailsService {

}




