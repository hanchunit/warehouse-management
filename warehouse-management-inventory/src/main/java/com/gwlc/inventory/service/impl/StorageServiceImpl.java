package com.gwlc.inventory.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gwlc.inventory.domain.dto.StorageDTO;
import com.gwlc.inventory.domain.entity.StorageDO;
import com.gwlc.inventory.domain.query.StorageQuery;
import com.gwlc.inventory.domain.vo.SaveStorageVO;
import com.gwlc.inventory.mapper.StorageMapper;
import com.gwlc.inventory.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * @author hanchun
 */
@Service
public class StorageServiceImpl extends ServiceImpl<StorageMapper, StorageDO>
        implements StorageService {
    @Autowired
    StorageMapper storageMapper;

    /**
     * 模糊查询入库单列表
     *
     * @param storage 入库单信息
     * @return 结果
     */
    @Override
    public List<StorageDTO> listStorages(StorageQuery storage) {
        return storageMapper.listStorages(storage);
    }

    /**
     * 精确查询入库单
     *
     * @param storageId 入库单标识
     * @return 结果
     */
    @Override
    public StorageDTO getStorage(Serializable storageId) {
        return storageMapper.getStorage(storageId);
    }

    /**
     * 保存入库单
     *
     * @param saveStorageDetails 入库单
     * @return 结果
     */
    @Override
    public Boolean saveStorageService(SaveStorageVO saveStorageDetails) {

        if (storageMapper.saveStorage(saveStorageDetails) <= 0) {
            return false;
        }
        if (saveStorageDetails.getStorageDetails() == null || saveStorageDetails.getStorageDetails().isEmpty()) {
            return true;
        }

        return storageMapper.saveStorageDetails(saveStorageDetails) > 0;
    }
}




