package com.gwlc.inventory.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gwlc.inventory.domain.entity.ClassifyDO;

/**
 *
 */
public interface ClassifyService extends IService<ClassifyDO> {

}
