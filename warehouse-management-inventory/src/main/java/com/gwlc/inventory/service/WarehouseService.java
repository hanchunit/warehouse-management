package com.gwlc.inventory.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gwlc.inventory.domain.dto.WarehouseDTO;
import com.gwlc.inventory.domain.entity.WarehouseDO;
import com.gwlc.inventory.domain.page.WarehousePage;
import com.gwlc.inventory.domain.query.WarehouseQuery;
import com.gwlc.inventory.domain.vo.SaveWarehouseCommodityVO;
import com.gwlc.inventory.domain.vo.SaveWarehouseVO;

import java.io.Serializable;
import java.util.List;

/**
 *
 */
public interface WarehouseService extends IService<WarehouseDO> {

    /**
     * 仓库列表
     *
     * @param warehouse 仓库信息
     * @return 结果
     */
    List<WarehouseDTO> listWarehouses(WarehouseQuery warehouse);

    /**
     * 分页
     *
     * @param warehouse 仓库
     * @return 结果
     */
    WarehousePage pageWarehouse(WarehousePage warehouse);

    /**
     * 根据标识获取仓库信息
     *
     * @param warehouseId 仓库标识
     * @return 结果
     */
    WarehouseDTO getWarehouse(Serializable warehouseId);

    /**
     * 保存仓库信息
     *
     * @param warehouse 仓库信息
     * @return 结果
     */
    Boolean saveWarehouse(SaveWarehouseVO warehouse);

    /**
     * 更新仓库信息
     *
     * @param warehouse 仓库
     * @return 结果
     */
    Boolean updateWarehouse(SaveWarehouseVO warehouse);

    /**
     * 根据标识删除仓库
     *
     * @param warehouseId 仓库标识
     * @return 结果
     */
    Boolean deleteWarehouse(Long warehouseId);

    /**
     * 统计仓库数量
     *
     * @param warehouse 仓库信息
     * @return 结果
     */
    Integer countWarehouse(WarehouseQuery warehouse);

    /**
     * 保存仓库商品信息
     *
     * @param saveWarehouseCommodity 仓库商品信息
     * @return 结果
     */
    Boolean saveWarehouseCommodities(SaveWarehouseCommodityVO saveWarehouseCommodity);

}
