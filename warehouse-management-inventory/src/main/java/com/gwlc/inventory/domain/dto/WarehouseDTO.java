package com.gwlc.inventory.domain.dto;

import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

import java.util.List;

/**
 * 仓库表
 *
 * @author hanchun
 * @TableName inventory_warehouse
 */

@Data
public class WarehouseDTO extends BaseSerializableObject {

    private static final long serialVersionUID = 8619659132731219798L;
    /**
     * 仓库标识
     */

    private Long warehouseId;

    /**
     * 仓库名称
     */

    private String warehouseName;

    /**
     * 仓库地址
     */
    private String warehouseSite;

    /**
     * 仓库状态
     */
    private Integer warehouseState;
    /**
     * 备注
     */
    private String remark;
    /**
     * 商品
     */
    private List<CommodityDTO> commodities;
}