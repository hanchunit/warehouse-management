package com.gwlc.inventory.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 入库详情
 *
 * @author hanchun
 * @TableName inventory_storage_details
 */
@TableName(value = "inventory_storage_details")
@Data
public class StorageDetailsDO extends BaseSerializableObject {
    private static final long serialVersionUID = 890178593929799870L;
    /**
     * 入库单标识
     */
    @TableId(value = "storage_id")
    private Long storageId;

    /**
     * 商品标识
     */
    @TableField(value = "commodity_id")
    private Long commodityId;


    /**
     * 数量
     */
    @TableField(value = "commodity_number")
    private Long commodityNumber;

    /**
     * 单价
     */
    @TableField(value = "commodity_price")
    private BigDecimal commodityPrice;

    /**
     * 总价
     */
    @TableField(value = "commodity_prices")
    private BigDecimal commodityPrices;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;

}