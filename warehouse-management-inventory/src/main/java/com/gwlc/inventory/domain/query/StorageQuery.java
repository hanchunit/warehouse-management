package com.gwlc.inventory.domain.query;

import com.gwlc.common.base.BaseSerializableObject;
import com.gwlc.inventory.domain.entity.StorageDO;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>warehouse-management-com.gwlc.inventory.domain.query-StorageQuery</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/3
 */
@Data
public class StorageQuery extends BaseSerializableObject {
    private static final long serialVersionUID = 7511404299913805219L;
    /**
     * 入库单标识
     */
    private Long storageId;

    /**
     * 客户标识
     */
    private Long clientId;

    /**
     * 仓库标识
     */
    private Long warehouseId;
    /**
     * 入库时间
     */
    private Date storageTime;

    /**
     * 总价
     */
    private BigDecimal storagePrice;

    /**
     * 物流
     */
    private String logistics;

    /**
     * 入库单状态
     */
    private Byte storageState;


    /**
     * 备注
     */
    private String remark;

    public StorageQuery() {
    }

    public StorageQuery(StorageDO storage) {
        if (storage != null) {
            this.storageId = storage.getStorageId();
            this.clientId = storage.getClientId();
            this.warehouseId = storage.getWarehouseId();
            this.storageTime = storage.getStorageTime();
            this.storagePrice = storage.getStoragePrice();
            this.logistics = storage.getLogistics();
            this.storageState = storage.getStorageState();
            this.remark = storage.getRemark();
        }
    }
}
