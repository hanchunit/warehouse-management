package com.gwlc.inventory.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gwlc.common.base.BaseEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * 入库表
 *
 * @author hanchun
 * @TableName inventory_storage
 */
@TableName(value = "inventory_storage")
@Data
public class StorageDO extends BaseEntity {
    private static final long serialVersionUID = -1044404132222619714L;
    /**
     * 入库标识
     */
    @TableId(value = "storage_id", type = IdType.AUTO)
    private Long storageId;

    /**
     * 客户标识
     */
    @TableField(value = "client_id")
    private Long clientId;

    /**
     * 仓库标识
     */
    @TableField(value = "warehouse_id")
    private Long warehouseId;
    /**
     * 入库时间
     */
    @TableField(value = "storage_time")
    private Date storageTime;

    /**
     * 总价
     */
    @TableField(value = "storage_price")
    private BigDecimal storagePrice;

    /**
     * 物流
     */
    @TableField(value = "logistics")
    private String logistics;

    /**
     * 入库单状态
     */
    @TableField(value = "storage_state")
    private Byte storageState;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private Date updateTime;

    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;

}