package com.gwlc.inventory.domain.vo;

import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

/**
 * <p>warehouse-management-com.gwlc.inventory.domain.vo.SaveWarehouseVO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/30
 */
@Data
public class SaveWarehouseVO extends BaseSerializableObject {


    private static final long serialVersionUID = -2363645918293164722L;
    /**
     * 备注
     */
    protected String remark;
    /**
     * 仓库标识
     */
    private Long warehouseId;
    /**
     * 仓库名称
     */
    private String warehouseName;
    /**
     * 仓库地址
     */
    private String warehouseSite;
    /**
     * 仓库状态
     */
    private Integer warehouseState;
}
