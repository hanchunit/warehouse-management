package com.gwlc.inventory.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gwlc.common.base.BaseEntity;
import lombok.Data;

/**
 * 分类表
 *
 * @TableName inventory_classify
 */
@TableName(value = "inventory_classify")
@Data
public class ClassifyDO extends BaseEntity {
    private static final long serialVersionUID = -2221954492646395264L;
    /**
     * 分类标识
     */
    @TableId(value = "classify_id", type = IdType.AUTO)
    private Long classifyId;

    /**
     * 分类名称
     */
    @TableField(value = "classify_name")
    private String classifyName;

    /**
     * 分类父标识
     */
    @TableField(value = "parent_classify")
    private Long parentClassify;

    /**
     * 分类状态
     */
    @TableField(value = "classify_state")
    private Integer classifyState;

}