package com.gwlc.inventory.domain.entity;

import com.gwlc.common.base.BaseEntity;
import lombok.Data;

/**
 * <p>warehouse-management-com.gwlc.inventory.domain.vo-WarehouseCommodityDO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/2
 */
@Data
public class WarehouseCommodityDO extends BaseEntity {
    private static final long serialVersionUID = 2717120054179665479L;


    private Long warehouseId;

    private Long commodityId;

    private Long commodityNumber;
}
