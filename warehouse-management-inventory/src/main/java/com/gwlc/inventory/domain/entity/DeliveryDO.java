package com.gwlc.inventory.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gwlc.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 入库表
 *
 * @TableName inventory_delivery
 */
@TableName(value = "inventory_delivery")
@Data
public class DeliveryDO extends BaseEntity implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * 出库标识
     */
    @TableId(value = "delivery_id", type = IdType.AUTO)
    private Long deliveryId;
    /**
     * 客户标识
     */
    @TableField(value = "client_id")
    private Long clientId;
    /**
     * 仓库标识
     */
    @TableField(value = "warehouse_id")
    private Long warehouseId;
    /**
     * 出库时间
     */
    @TableField(value = "delivery_time")
    private Date deliveryTime;
    /**
     * 总价
     */
    @TableField(value = "delivery_price")
    private BigDecimal deliveryPrice;
    /**
     * 物流
     */
    @TableField(value = "logistics")
    private String logistics;
    /**
     * 出库单状态
     */
    @TableField(value = "delivery_state")
    private Byte deliveryState;
    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;
    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private Date updateTime;
    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;
}