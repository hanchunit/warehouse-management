package com.gwlc.inventory.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gwlc.common.base.BaseEntity;
import lombok.Data;

/**
 * 仓库表
 *
 * @TableName inventory_warehouse
 */
@TableName(value = "inventory_warehouse")
@Data
public class WarehouseDO extends BaseEntity {
    private static final long serialVersionUID = 7825862665786447613L;
    /**
     * 仓库标识
     */
    @TableId(value = "warehouse_id", type = IdType.AUTO)
    private Long warehouseId;

    /**
     * 仓库名称
     */
    @TableField(value = "warehouse_name")
    private String warehouseName;

    /**
     * 仓库地址
     */
    @TableField(value = "warehouse_site")
    private String warehouseSite;

    /**
     * 仓库状态
     */
    @TableField(value = "warehouse_state")
    private Integer warehouseState;

}