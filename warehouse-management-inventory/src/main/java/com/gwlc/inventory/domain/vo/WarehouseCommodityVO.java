package com.gwlc.inventory.domain.vo;

import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

/**
 * <p>warehouse-management-com.gwlc.inventory.domain.vo.SaveWarehouseVO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/30
 */
@Data
public class WarehouseCommodityVO extends BaseSerializableObject {


    private static final long serialVersionUID = -2616613775965885609L;
    private Long commodityId;
    private Long commodityNumber;
}
