package com.gwlc.inventory.domain.query;

import com.gwlc.common.base.BaseSerializableObject;
import com.gwlc.inventory.domain.entity.CommodityDO;
import lombok.Data;

/**
 * 商品表
 *
 * @author hanchun
 * @TableName inventory_commodity
 */

@Data
public class CommodityQuery extends BaseSerializableObject {

    private static final long serialVersionUID = -110770237829579049L;
    /**
     * 商品标识
     */
    private Long commodityId;

    /**
     * 品名
     */
    private String commodityName;

    /**
     * 品牌
     */
    private String commodityBrand;

    /**
     * 规格
     */
    private String commoditySpecification;

    /**
     * 供应商
     */
    private String commoditySupplier;

    public CommodityQuery() {
    }

    public CommodityQuery(CommodityDO commodity) {
        if (commodity != null) {
            this.commodityId = commodity.getCommodityId();
            this.commodityName = commodity.getCommodityName();
            this.commodityBrand = commodity.getCommodityBrand();
            this.commoditySpecification = commodity.getCommoditySpecification();
            this.commoditySupplier = commodity.getCommoditySupplier();
        }
    }

    public CommodityQuery(Long commodityId) {
        this.commodityId = commodityId;
    }
}