package com.gwlc.inventory.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gwlc.common.base.BaseEntity;
import lombok.Data;

/**
 * 商品表
 *
 * @TableName inventory_commodity
 */
@TableName(value = "inventory_commodity")
@Data
public class CommodityDO extends BaseEntity {
    private static final long serialVersionUID = 525258443036651336L;
    /**
     * 商品标识
     */
    @TableId(value = "commodity_id", type = IdType.AUTO)
    private Long commodityId;

    /**
     * 品名
     */
    @TableField(value = "commodity_name")
    private String commodityName;

    /**
     * 品牌
     */
    @TableField(value = "commodity_brand")
    private String commodityBrand;

    /**
     * 规格
     */
    @TableField(value = "commodity_specification")
    private String commoditySpecification;

    /**
     * 供应商
     */
    @TableField(value = "commodity_supplier")
    private String commoditySupplier;

}