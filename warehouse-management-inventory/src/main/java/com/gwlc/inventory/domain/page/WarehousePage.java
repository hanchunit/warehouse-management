package com.gwlc.inventory.domain.page;

import com.gwlc.common.base.BasePage;
import com.gwlc.inventory.domain.dto.WarehouseDTO;
import com.gwlc.inventory.domain.query.WarehouseQuery;

/**
 * <p>warehouse-management-com.gwlc.inventory.domain.page-WarehousePage</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/11
 */


public class WarehousePage extends BasePage<WarehouseDTO, WarehouseQuery> {


    private static final long serialVersionUID = -2127811038351297337L;

    @Override
    public String toString() {
        return "WarehousePage{} " + super.toString();
    }
}
