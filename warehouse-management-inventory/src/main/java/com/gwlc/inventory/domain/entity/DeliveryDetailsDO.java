package com.gwlc.inventory.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gwlc.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 入库详情
 *
 * @TableName inventory_delivery_details
 */
@TableName(value = "inventory_delivery_details")
@Data
public class DeliveryDetailsDO extends BaseEntity implements Serializable {
    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    /**
     * 出库单标识
     */
    @TableId(value = "delivery_id")
    private Long deliveryId;
    /**
     * 商品标识
     */
    @TableField(value = "commodity_id")
    private Long commodityId;
    /**
     * 单价
     */
    @TableField(value = "commodity_price")
    private BigDecimal commodityPrice;
    /**
     * 数量
     */
    @TableField(value = "commodity_number")
    private Long commodityNumber;
    /**
     * 总价
     */
    @TableField(value = "commodity_prices")
    private BigDecimal commodityPrices;
    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    private Date createTime;
    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    private Date updateTime;
    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;
}