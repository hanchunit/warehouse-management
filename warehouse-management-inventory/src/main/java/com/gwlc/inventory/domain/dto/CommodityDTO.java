package com.gwlc.inventory.domain.dto;

import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

import java.util.List;

/**
 * 商品表
 *
 * @author hanchun
 * @TableName inventory_commodity
 */

@Data
public class CommodityDTO extends BaseSerializableObject {
    private static final long serialVersionUID = -4063352872853697793L;
    /**
     * 商品标识
     */
    private Long commodityId;

    /**
     * 品名
     */
    private String commodityName;

    /**
     * 品牌
     */
    private String commodityBrand;

    /**
     * 规格
     */
    private String commoditySpecification;

    /**
     * 供应商
     */
    private String commoditySupplier;
    /**
     * 分类列表
     */
    private List<ClassifyDTO> classifies;
}