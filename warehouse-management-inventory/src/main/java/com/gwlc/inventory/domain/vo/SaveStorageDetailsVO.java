package com.gwlc.inventory.domain.vo;

import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>warehouse-management-com.gwlc.inventory.domain.vo-SaveStorageDetailsVO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/3
 */
@Data
public class SaveStorageDetailsVO extends BaseSerializableObject {
    private static final long serialVersionUID = 609183892572078145L;


    /**
     * 商品标识
     */
    private Long commodityId;


    /**
     * 数量
     */
    private Long commodityNumber;

    /**
     * 单价
     */
    private BigDecimal commodityPrice;

    /**
     * 总价
     */
    private BigDecimal commodityPrices;


    /**
     * 备注
     */
    private String remark;

}
