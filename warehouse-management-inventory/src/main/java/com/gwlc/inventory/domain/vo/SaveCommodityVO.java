package com.gwlc.inventory.domain.vo;

import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>warehouse-management-com.gwlc.inventory.domain.dto-SaveCommodityVO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/28
 */
@Data
public class SaveCommodityVO extends BaseSerializableObject {
    private static final long serialVersionUID = 633281577658821740L;
    /**
     * 商品标识
     */
    private Long commodityId;
    /**
     * 品名
     */
    private String commodityName;

    /**
     * 品牌
     */
    private String commodityBrand;

    /**
     * 规格
     */
    private String commoditySpecification;

    /**
     * 供应商
     */
    private String commoditySupplier;
    /**
     * 分类列表
     */
    private List<Long> classifies;

    public SaveCommodityVO() {
    }

    public SaveCommodityVO(Long commodityId, String commodityName, String commodityBrand, String commoditySpecification, String commoditySupplier, List<Long> classifies) {
        this.commodityId = commodityId;
        this.commodityName = commodityName;
        this.commodityBrand = commodityBrand;
        this.commoditySpecification = commoditySpecification;
        this.commoditySupplier = commoditySupplier;
        this.classifies = classifies;
    }

    public SaveCommodityVO(String commodityName, String commodityBrand, String commoditySpecification, String commoditySupplier, Long[] classifies) {
        this.commodityName = commodityName;
        this.commodityBrand = commodityBrand;
        this.commoditySpecification = commoditySpecification;
        this.commoditySupplier = commoditySupplier;
        if (classifies != null) {
            this.classifies = List.of(classifies);
        } else {
            this.classifies = new ArrayList<>();
        }
    }

    public SaveCommodityVO(String commodityName, String commodityBrand, String commoditySpecification, String commoditySupplier, List<Long> classifies) {
        this.commodityName = commodityName;
        this.commodityBrand = commodityBrand;
        this.commoditySpecification = commoditySpecification;
        this.commoditySupplier = commoditySupplier;
        this.classifies = classifies;
    }
}
