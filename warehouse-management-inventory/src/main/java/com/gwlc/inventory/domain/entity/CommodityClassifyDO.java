package com.gwlc.inventory.domain.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.gwlc.common.base.BaseEntity;
import lombok.Data;

/**
 * 商品表
 *
 * @author hanchun
 * @TableName inventory_commodity
 */
@TableName(value = "inventory_commodity_classify")
@Data
public class CommodityClassifyDO extends BaseEntity {
    private static final long serialVersionUID = 525258443036651336L;
    /**
     * 商品标识
     */
    private Long commodityId;

    /**
     * 分类标识
     */
    private Long classifyId;

    public CommodityClassifyDO() {
    }

    public CommodityClassifyDO(Long commodityId, Long classifyId) {
        this.commodityId = commodityId;
        this.classifyId = classifyId;
    }
}