package com.gwlc.inventory.domain.query;

import com.gwlc.common.base.BaseSerializableObject;
import com.gwlc.inventory.domain.entity.WarehouseDO;
import lombok.Data;

/**
 * <p>warehouse-management-com.gwlc.inventory.domain.query-WarehouseQuery</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/30
 */
@Data
public class WarehouseQuery extends BaseSerializableObject {
    private static final long serialVersionUID = 7605262516135215062L;

    /**
     * 仓库标识
     */
    private Long warehouseId;

    /**
     * 仓库名称
     */
    private String warehouseName;

    /**
     * 仓库地址
     */
    private String warehouseSite;

    /**
     * 仓库状态
     */

    private Integer warehouseState;
    /**
     * 备注
     */
    private String remark;

    public WarehouseQuery() {
    }

    public WarehouseQuery(WarehouseDO warehouse) {
        if (warehouse != null) {
            this.warehouseId = warehouse.getWarehouseId();
            this.warehouseName = warehouse.getWarehouseName();
            this.warehouseSite = warehouse.getWarehouseSite();
            this.warehouseState = warehouse.getWarehouseState();
            this.remark = warehouse.getRemark();
        }
    }
}
