package com.gwlc.inventory.domain.page;

import com.gwlc.common.base.BasePage;
import com.gwlc.inventory.domain.dto.DeliveryDTO;
import com.gwlc.inventory.domain.query.DeliveryQuery;

/**
 * <p>warehouse-management-com.gwlc.inventory.domain.page-DeliveryPage</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/11
 */


public class DeliveryPage extends BasePage<DeliveryDTO, DeliveryQuery> {


    private static final long serialVersionUID = 3317188231346715171L;

    @Override
    public String toString() {
        return "DeliveryPage{} " + super.toString();
    }
}
