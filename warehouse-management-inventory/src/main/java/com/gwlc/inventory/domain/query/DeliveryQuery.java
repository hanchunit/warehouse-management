package com.gwlc.inventory.domain.query;

import com.gwlc.common.base.BaseSerializableObject;
import com.gwlc.inventory.domain.entity.DeliveryDO;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * <p>warehouse-management-com.gwlc.inventory.domain.query-StorageQuery</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/3
 */
@Data
public class DeliveryQuery extends BaseSerializableObject {

    private static final long serialVersionUID = -3193944832744213104L;
    /**
     * 出库单标识
     */
    private Long deliveryId;

    /**
     * 客户标识
     */
    private Long clientId;

    /**
     * 仓库标识
     */
    private Long warehouseId;
    /**
     * 出库时间
     */
    private Date deliveryTime;

    /**
     * 总价
     */
    private BigDecimal deliveryPrice;

    /**
     * 物流
     */
    private String logistics;

    /**
     * 出库单状态
     */
    private Byte deliveryState;


    /**
     * 备注
     */
    private String remark;


    public DeliveryQuery() {
    }

    public DeliveryQuery(DeliveryDO delivery) {
        if (delivery != null) {
            this.deliveryId = delivery.getDeliveryId();
            this.clientId = delivery.getClientId();
            this.warehouseId = delivery.getWarehouseId();
            this.deliveryTime = delivery.getDeliveryTime();
            this.deliveryPrice = delivery.getDeliveryPrice();
            this.logistics = delivery.getLogistics();
            this.deliveryState = delivery.getDeliveryState();
            this.remark = delivery.getRemark();
        }
    }
}
