package com.gwlc.inventory.domain.vo;

import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

import java.util.List;

/**
 * <p>warehouse-management-com.gwlc.inventory.domain.dto-SaveCommodityVO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/28
 */
@Data
public class EditCommodityVO extends BaseSerializableObject {

    private static final long serialVersionUID = 4442527938618593319L;
    /**
     * 商品标识
     */
    private Long commodityId;
    /**
     * 品名
     */
    private String commodityName;

    /**
     * 品牌
     */
    private String commodityBrand;

    /**
     * 规格
     */
    private String commoditySpecification;

    /**
     * 供应商
     */
    private String commoditySupplier;
    /**
     * 新增分类列表
     */
    private List<Long> saveClassifies;
    /**
     * 删除分类列表
     */
    private List<Long> deleteClassifies;

    public EditCommodityVO() {
    }


    public EditCommodityVO(String commodityName, String commodityBrand, String commoditySpecification, String commoditySupplier, List<Long> saveClassifies, List<Long> deleteClassifies) {
        this.commodityName = commodityName;
        this.commodityBrand = commodityBrand;
        this.commoditySpecification = commoditySpecification;
        this.commoditySupplier = commoditySupplier;
        this.saveClassifies = saveClassifies;
        this.deleteClassifies = deleteClassifies;
    }

    public EditCommodityVO(Long commodityId, String commodityName, String commodityBrand, String commoditySpecification, String commoditySupplier, List<Long> saveClassifies, List<Long> deleteClassifies) {
        this.commodityId = commodityId;
        this.commodityName = commodityName;
        this.commodityBrand = commodityBrand;
        this.commoditySpecification = commoditySpecification;
        this.commoditySupplier = commoditySupplier;
        this.saveClassifies = saveClassifies;
        this.deleteClassifies = deleteClassifies;
    }
}
