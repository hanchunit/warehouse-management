package com.gwlc.inventory.domain.dto;

import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>warehouse-management-com.gwlc.inventory.domain.dto-StorageDTO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/3
 */
@Data
public class StorageDTO extends BaseSerializableObject {
    private static final long serialVersionUID = 3065898007456508451L;


    /**
     * 入库标识
     */
    private Long storageId;

    /**
     * 客户标识
     */
    private Long clientId;

    /**
     * 仓库标识
     */
    private Long warehouseId;
    /**
     * 入库时间
     */
    private Date storageTime;

    /**
     * 总价
     */
    private BigDecimal storagePrice;

    /**
     * 物流
     */
    private String logistics;

    /**
     * 入库单状态
     */
    private Byte storageState;

    /**
     * 备注
     */

    private String remark;

    /**
     * 入库详情
     */
    private List<StorageDetailsDTO> storageDetails;
}
