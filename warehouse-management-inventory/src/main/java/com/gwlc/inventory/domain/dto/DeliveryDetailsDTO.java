package com.gwlc.inventory.domain.dto;

import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>warehouse-management-com.gwlc.inventory.domain.dto-StorageDetailsDTO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/3
 */
@Data
public class DeliveryDetailsDTO extends BaseSerializableObject {


    private static final long serialVersionUID = 311298736734954090L;
    /**
     * 出库单标识
     */
    private Long deliveryId;

    /**
     * 商品标识
     */
    private Long commodityId;


    /**
     * 数量
     */
    private Long commodityNumber;

    /**
     * 单价
     */
    private BigDecimal commodityPrice;

    /**
     * 总价
     */
    private BigDecimal commodityPrices;


    /**
     * 备注
     */
    private String remark;
}
