package com.gwlc.inventory.domain.dto;

import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

/**
 * 分类表
 *
 * @author hanchun
 * @TableName inventory_classify
 */

@Data
public class ClassifyDTO extends BaseSerializableObject {

    private static final long serialVersionUID = 5941690372255026410L;
    /**
     * 分类标识
     */

    private Long classifyId;

    /**
     * 分类名称
     */

    private String classifyName;

    /**
     * 分类父标识
     */

    private Long parentClassify;

    /**
     * 分类状态
     */

    private Integer classifyState;

}