package com.gwlc.inventory.domain.page;

import com.gwlc.common.base.BasePage;
import com.gwlc.inventory.domain.dto.CommodityDTO;
import com.gwlc.inventory.domain.query.CommodityQuery;

/**
 * <p>warehouse-management-com.gwlc.inventory.domain.page-CommodityPage</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/11
 */


public class CommodityPage extends BasePage<CommodityDTO, CommodityQuery> {
    private static final long serialVersionUID = -3353351471312888072L;

    @Override
    public String toString() {
        return "CommodityPage{} " + super.toString();
    }
}
