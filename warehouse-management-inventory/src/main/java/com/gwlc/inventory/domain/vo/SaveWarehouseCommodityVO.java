package com.gwlc.inventory.domain.vo;

import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

import java.util.List;

/**
 * <p>warehouse-management-com.gwlc.inventory.domain.vo.SaveWarehouseVO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/30
 */
@Data
public class SaveWarehouseCommodityVO extends BaseSerializableObject {


    private static final long serialVersionUID = -2504215762193228969L;
    /**
     * 仓库标识
     */
    private Long warehouseId;


    private List<WarehouseCommodityVO> commodities;
}
