package com.gwlc.inventory.domain.vo;

import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p>warehouse-management-com.gwlc.inventory.domain.vo-SaveStorageVO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/3
 */
@Data
public class SaveDeliveryVO extends BaseSerializableObject {


    private static final long serialVersionUID = -815704854106029135L;

    /**
     * 出库单标识
     */
    private Long deliveryId;

    /**
     * 客户标识
     */
    private Long clientId;

    /**
     * 仓库标识
     */
    private Long warehouseId;
    /**
     * 出库时间
     */
    private Date deliveryTime;

    /**
     * 总价
     */
    private BigDecimal deliveryPrice;

    /**
     * 物流
     */
    private String logistics;

    /**
     * 出库单状态
     */
    private Byte deliveryState;

    /**
     * 备注
     */
    private String remark;
    /**
     * 入库详情
     */
    private List<SaveDeliveryDetailsVO> deliveryDetails;

}
