package com.gwlc.inventory.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gwlc.inventory.domain.dto.CommodityDTO;
import com.gwlc.inventory.domain.entity.CommodityClassifyDO;
import com.gwlc.inventory.domain.entity.CommodityDO;
import com.gwlc.inventory.domain.page.CommodityPage;
import com.gwlc.inventory.domain.query.CommodityQuery;
import com.gwlc.inventory.domain.vo.EditCommodityVO;
import com.gwlc.inventory.domain.vo.SaveCommodityVO;
import org.apache.ibatis.annotations.Mapper;

import java.io.Serializable;
import java.util.List;

/**
 * @author hanchun
 * @Entity com.gwlc.inventory.domain.entity.CommodityDO
 */
@Mapper
public interface CommodityMapper extends BaseMapper<CommodityDO> {
    /**
     * 查询商品列表
     *
     * @param commodity 查询条件
     * @return 商品列表
     */
    List<CommodityDTO> listCommodities(CommodityQuery commodity);

    /**
     * 根据标识获取商品信息
     *
     * @param commodityId 商品标识
     * @return 商品
     */
    CommodityDTO getCommodityById(Serializable commodityId);

    /**
     * 保存商品信息
     *
     * @param saveCommodity 商品信息
     * @return 结果
     */
    int saveCommodity(SaveCommodityVO saveCommodity);

    /**
     * 保存商品分类信息
     *
     * @param commodityClassifies 商品信息
     * @return 结果
     */
    int saveCommodityClassify(List<CommodityClassifyDO> commodityClassifies);

    /**
     * 更新商品信息
     *
     * @param editCommodity 商品信息
     * @return 结果
     */
    int updateCommodity(EditCommodityVO editCommodity);

    /**
     * 删除商品信息
     *
     * @param commodityId 商品标识
     * @return 结果
     */
    int deleteCommodityById(Serializable commodityId);

    /**
     * 删除商品分类信息
     *
     * @param commodityId 商品标识
     * @return 结果
     */
    int deleteCommodityClassifyById(Serializable commodityId);

    /**
     * 删除商品分类信息
     *
     * @param commodityClassifies 商品分类信息
     * @return 结果
     */
    int deleteCommodityClassify(List<CommodityClassifyDO> commodityClassifies);


    /**
     * 统计符合条件的商品数量
     *
     * @param commodity 商品条件
     * @return 结果
     */
    int countByCommodity(CommodityQuery commodity);

    /**
     * 模糊统计符合条件的商品数量
     *
     * @param commodity 商品条件
     * @return 结果
     */
    int countVagueByCommodity(CommodityQuery commodity);

    /**
     * 统计符合条件的商品分类数量
     *
     * @param commodityClassify 商品分类条件
     * @return 结果
     */
    int countByCommodityClassify(CommodityClassifyDO commodityClassify);

    /**
     * 分页查询
     *
     * @param commodity 分页
     * @return 结果
     */
    List<CommodityDTO> page(CommodityPage commodity);
}




