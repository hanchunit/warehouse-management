package com.gwlc.inventory.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gwlc.inventory.domain.dto.StorageDTO;
import com.gwlc.inventory.domain.entity.StorageDO;
import com.gwlc.inventory.domain.query.StorageQuery;
import com.gwlc.inventory.domain.vo.SaveStorageVO;
import org.apache.ibatis.annotations.Mapper;

import java.io.Serializable;
import java.util.List;

/**
 * @author hanchun
 * @Entity com.gwlc.inventory.domain.entity.StorageDO
 */
@Mapper
public interface StorageMapper extends BaseMapper<StorageDO> {
    /**
     * 保存入库单
     *
     * @param saveStorageDetails 入库单
     * @return 结果
     */
    int saveStorage(SaveStorageVO saveStorageDetails);

    /**
     * 保存入库单详情
     *
     * @param saveStorageDetails 入库单详情
     * @return 结果
     */
    int saveStorageDetails(SaveStorageVO saveStorageDetails);

    /**
     * 模糊查询入库单列表
     *
     * @param storage 入库单信息
     * @return 结果
     */
    List<StorageDTO> listStorages(StorageQuery storage);

    /**
     * 根据标识精准查询入库单
     *
     * @param storageId 入库单标识
     * @return 结果
     */
    StorageDTO getStorage(Serializable storageId);
}




