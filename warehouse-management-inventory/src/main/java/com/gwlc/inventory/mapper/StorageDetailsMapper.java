package com.gwlc.inventory.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gwlc.inventory.domain.entity.StorageDetailsDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author hanchun
 * @Entity com.gwlc.inventory.domain.entity.StorageDetailsDO
 */
@Mapper
public interface StorageDetailsMapper extends BaseMapper<StorageDetailsDO> {

}




