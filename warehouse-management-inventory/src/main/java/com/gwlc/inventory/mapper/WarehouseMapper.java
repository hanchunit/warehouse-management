package com.gwlc.inventory.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gwlc.inventory.domain.dto.WarehouseDTO;
import com.gwlc.inventory.domain.entity.WarehouseDO;
import com.gwlc.inventory.domain.page.WarehousePage;
import com.gwlc.inventory.domain.query.WarehouseQuery;
import com.gwlc.inventory.domain.vo.SaveWarehouseCommodityVO;
import com.gwlc.inventory.domain.vo.SaveWarehouseVO;
import org.apache.ibatis.annotations.Mapper;

import java.io.Serializable;
import java.util.List;

/**
 * @author hanchun
 * @Entity com.gwlc.inventory.domain.entity.WarehouseDO
 */
@Mapper
public interface WarehouseMapper extends BaseMapper<WarehouseDO> {
    /**
     * 仓库列表
     *
     * @param warehouse 仓库信息
     * @return 结果
     */
    List<WarehouseDTO> listWarehouses(WarehouseQuery warehouse);

    /**
     * 根据标识获取仓库信息
     *
     * @param warehouseId 仓库标识
     * @return 结果
     */
    WarehouseDTO getWarehouseById(Serializable warehouseId);


    /**
     * 保存仓库
     *
     * @param saveWarehouse 仓库信息
     * @return 结果
     */
    int saveWarehouse(SaveWarehouseVO saveWarehouse);

    /**
     * 更新仓库
     *
     * @param saveWarehouse 仓库
     * @return 结果
     */
    int updateWarehouse(SaveWarehouseVO saveWarehouse);

    /**
     * 删除仓库
     *
     * @param warehouseId 仓库标识
     * @return 结果
     */
    int deleteWarehouse(Serializable warehouseId);

    /**
     * 统计仓库信息
     *
     * @param warehouse 仓库
     * @return 结果
     */
    int countByWarehouse(WarehouseQuery warehouse);

    /**
     * 保存仓库商品信息
     *
     * @param saveWarehouseCommodity 仓库商品信息
     * @return 结果
     */
    int saveWarehouseCommodities(SaveWarehouseCommodityVO saveWarehouseCommodity);


    /**
     * 分页查询
     *
     * @param warehouse 分页
     * @return 结果
     */
    List<WarehouseDTO> page(WarehousePage warehouse);


    /**
     * 模糊统计符合条件的仓库数量
     *
     * @param warehouse 仓库条件
     * @return 结果
     */
    int countVagueByWarehouse(WarehouseQuery warehouse);

}




