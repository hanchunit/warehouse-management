package com.gwlc.inventory.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gwlc.inventory.domain.entity.DeliveryDetailsDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author hanchun
 * @Entity com.gwlc.inventory.domain.entity.DeliveryDetailsDO
 */
@Mapper
public interface DeliveryDetailsMapper extends BaseMapper<DeliveryDetailsDO> {

}




