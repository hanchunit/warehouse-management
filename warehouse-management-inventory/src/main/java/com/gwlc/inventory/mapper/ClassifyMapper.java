package com.gwlc.inventory.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gwlc.inventory.domain.entity.ClassifyDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.gwlc.inventory.domain.entity.ClassifyDO
 */

@Mapper
public interface ClassifyMapper extends BaseMapper<ClassifyDO> {

}




