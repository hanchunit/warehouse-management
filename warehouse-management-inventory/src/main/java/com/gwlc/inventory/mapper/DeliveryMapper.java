package com.gwlc.inventory.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gwlc.inventory.domain.dto.DeliveryDTO;
import com.gwlc.inventory.domain.entity.DeliveryDO;
import com.gwlc.inventory.domain.page.DeliveryPage;
import com.gwlc.inventory.domain.query.DeliveryQuery;
import com.gwlc.inventory.domain.vo.SaveDeliveryVO;
import org.apache.ibatis.annotations.Mapper;

import java.io.Serializable;
import java.util.List;

/**
 * @author hanchun
 * @Entity com.gwlc.inventory.domain.entity.DeliveryDO
 */
@Mapper
public interface DeliveryMapper extends BaseMapper<DeliveryDO> {

    /**
     * 保存出库单
     *
     * @param saveDeliveryDetails 出库单
     * @return 结果
     */
    int saveDelivery(SaveDeliveryVO saveDeliveryDetails);

    /**
     * 保存出库单详情
     *
     * @param saveDeliveryDetails 出库单详情
     * @return 结果
     */
    int saveDeliveryDetails(SaveDeliveryVO saveDeliveryDetails);

    /**
     * 模糊查询出库单列表
     *
     * @param delivery 出库单信息
     * @return 结果
     */
    List<DeliveryDTO> listDeliveries(DeliveryQuery delivery);

    /**
     * 根据标识精准查询出库单
     *
     * @param deliveryId 出库单标识
     * @return 结果
     */
    DeliveryDTO getDelivery(Serializable deliveryId);


    /**
     * 分页查询
     *
     * @param delivery 分页
     * @return 结果
     */
    List<DeliveryDTO> page(DeliveryPage delivery);


    /**
     * 模糊统计符合条件的出库单数量
     *
     * @param delivery 出库单条件
     * @return 结果
     */
    int countVagueByDelivery(DeliveryQuery delivery);


    /**
     * 更新出库单
     *
     * @param saveDelivery 出库单
     * @return 结果
     */
    int updateDelivery(SaveDeliveryVO saveDelivery);

    /**
     * 删除出库单
     *
     * @param deliveryId 出库单标识
     * @return 结果
     */
    int deleteDelivery(Serializable deliveryId);

}




