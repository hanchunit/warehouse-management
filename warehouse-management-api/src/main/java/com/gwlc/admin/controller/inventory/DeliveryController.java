package com.gwlc.admin.controller.inventory;

import com.gwlc.common.result.Result;
import com.gwlc.inventory.domain.dto.DeliveryDTO;
import com.gwlc.inventory.domain.entity.DeliveryDO;
import com.gwlc.inventory.domain.page.DeliveryPage;
import com.gwlc.inventory.domain.query.DeliveryQuery;
import com.gwlc.inventory.domain.vo.SaveDeliveryVO;
import com.gwlc.inventory.service.DeliveryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>warehouse-management-com.gwlc.admin.controller.inventory-DeliveryController</p>
 * <p>Description:  入库</p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/3
 */
@RestController
@RequestMapping("/inventory/delivery")
public class DeliveryController {

    @Autowired
    DeliveryService deliveryService;

    /**
     * 查询入库单列表
     *
     * @param delivery 入库单信息
     * @return 入库单列表
     */
    @PostMapping("/get_list_deliveries")
    public Result<List<DeliveryDTO>> listDeliveries(@RequestBody(required = false) DeliveryDO delivery) {


        return Result.ok(deliveryService.listDeliveries(new DeliveryQuery(delivery)));
    }


    @PostMapping("/get_delivery_page")
    public Result<DeliveryPage> pageDelivery(@RequestBody(required = false) DeliveryPage delivery) {
        if (delivery == null) {
            delivery = new DeliveryPage();
        }
        delivery.addLastLimit();
        System.out.println(delivery);
        return Result.ok(deliveryService.pageDelivery(delivery));
    }

    /**
     * 查询入库单信息
     *
     * @param deliveryId 入库单标识
     * @return 入库单信息
     */
    @GetMapping("/get_delivery/{deliveryId}")
    public Result<DeliveryDTO> listDeliveries(@PathVariable Serializable deliveryId) {


        return Result.ok(deliveryService.getDelivery(deliveryId));
    }

    /**
     * 保存入库单信息
     *
     * @param saveDeliveryDetails 入库单信息
     * @return 结果
     */
    @PostMapping("/save_delivery")
    public Result<String> saveDelivery(@RequestBody SaveDeliveryVO saveDeliveryDetails) {

        return Result.okOrFailed(deliveryService.saveDeliveryService(saveDeliveryDetails), "入库单保存成功", "入库单保存失败");
    }


    /**
     * 修改入库单
     *
     * @param delivery 入库单信息
     * @return 结果
     */
    @PostMapping("/edit_delivery")
    public Result<String> editWarehouse(@RequestBody SaveDeliveryVO delivery) {
        System.out.println(delivery);
        return Result.okOrFailed(deliveryService.updateDelivery(delivery), "修改入库单成功", "修改入库单失败");

    }

    /**
     * 删除入库单
     *
     * @param warehouseId 仓库标识
     * @return 结果
     */
    @GetMapping("/delete_delivery/{deliveryId}")
    public Result<String> deleteDelivery(@PathVariable Long deliveryId) {
        return Result.okOrFailed(deliveryService.deleteDelivery(deliveryId), "删除入库单成功", "删除入库单失败");
    }

}
