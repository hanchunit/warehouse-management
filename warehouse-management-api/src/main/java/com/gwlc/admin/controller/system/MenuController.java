package com.gwlc.admin.controller.system;

import com.gwlc.common.annotation.PermissionAnnotation;
import com.gwlc.common.result.Result;
import com.gwlc.system.domain.dto.MenuTree;
import com.gwlc.system.domain.entity.MenuDO;
import com.gwlc.system.domain.query.MenuQuery;
import com.gwlc.system.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>warehouse-management-com.gwlc.admin.controller.system-MenuController</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/7
 */
@RequestMapping("/menu")
@RestController
public class MenuController {

    @Autowired
    IMenuService menuService;

    @PermissionAnnotation
    @PostMapping("/get_list_menus")
    public Result<List<MenuTree>> getListMenus(@RequestBody(required = false) MenuDO menu) {


        return Result.ok(menuService.listMenus(new MenuQuery(menu)));

    }
}
