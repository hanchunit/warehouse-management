package com.gwlc.admin.controller.inventory;

import com.gwlc.common.result.Result;
import com.gwlc.inventory.domain.dto.StorageDTO;
import com.gwlc.inventory.domain.entity.StorageDO;
import com.gwlc.inventory.domain.query.StorageQuery;
import com.gwlc.inventory.domain.vo.SaveStorageVO;
import com.gwlc.inventory.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>warehouse-management-com.gwlc.admin.controller.inventory-StorageController</p>
 * <p>Description:  入库单</p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/3
 */
@RestController
@RequestMapping("/inventory/storage")
public class StorageController {

    @Autowired
    StorageService storageService;

    /**
     * 查询入库单列表
     *
     * @param storage 入库单信息
     * @return 入库单列表
     */
    @PostMapping("/get_list_storages")
    public Result<List<StorageDTO>> listStorages(@RequestBody(required = false) StorageDO storage) {


        return Result.ok(storageService.listStorages(new StorageQuery(storage)));
    }

    /**
     * 查询入库单信息
     *
     * @param storageId 入库单标识
     * @return 入库单信息
     */
    @GetMapping("/get_storage/{storageId}")
    public Result<StorageDTO> listStorages(@PathVariable Serializable storageId) {


        return Result.ok(storageService.getStorage(storageId));
    }

    /**
     * 保存入库单信息
     *
     * @param saveStorageDetails 入库单信息
     * @return 结果
     */
    @PostMapping("/save_storage")
    public Result<String> saveStorage(@RequestBody SaveStorageVO saveStorageDetails) {

        return Result.okOrFailed(storageService.saveStorageService(saveStorageDetails), "入库单保存成功", "入库单保存失败");
    }


}
