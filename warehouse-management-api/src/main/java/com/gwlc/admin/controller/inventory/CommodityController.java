package com.gwlc.admin.controller.inventory;

import com.gwlc.common.annotation.PermissionAnnotation;
import com.gwlc.common.result.Result;
import com.gwlc.inventory.domain.dto.CommodityDTO;
import com.gwlc.inventory.domain.entity.CommodityDO;
import com.gwlc.inventory.domain.page.CommodityPage;
import com.gwlc.inventory.domain.query.CommodityQuery;
import com.gwlc.inventory.domain.vo.EditCommodityVO;
import com.gwlc.inventory.domain.vo.SaveCommodityVO;
import com.gwlc.inventory.service.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>warehouse-management-com.gwlc.admin.controller.inventory-CommodityController</p>
 * <p>Description: 商品控制器 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/28
 */
@RestController
@RequestMapping("/inventory/commodity")
public class CommodityController {


    @Autowired
    CommodityService commodityService;

    /**
     * 获取商品列表
     *
     * @param commodity 查询条件
     * @return 商品列表
     */
    @PostMapping("/get_list_commodities")
    public Result<List<CommodityDTO>> listCommodities(@RequestBody(required = false) CommodityDO commodity) {
        System.out.println(commodity);
        return Result.ok(commodityService.listCommodities(new CommodityQuery(commodity)));
    }

    @PostMapping("/get_commodities_page")
    public Result<CommodityPage> pageCommodities(@RequestBody(required = false) CommodityPage commodity) {
        commodity.addLastLimit();
        System.out.println(commodity);
        return Result.ok(commodityService.pageCommodities(commodity));
    }

    /**
     * 获取商品
     *
     * @param commodityId 商品标识
     * @return 商品列表
     */
    @PermissionAnnotation
    @GetMapping("/get_commodity/{commodityId}")
    public Result<CommodityDTO> getCommodityById(@PathVariable Serializable commodityId) {
        return Result.ok(commodityService.getCommodity(commodityId));
    }

    /**
     * 添加商品
     *
     * @param commodity 商品信息
     * @return 结果
     */
    @PostMapping("/save_commodity")
    public Result<String> saveCommodity(@RequestBody SaveCommodityVO commodity) {

        if (commodity.getCommodityName() == null) {
            return Result.failed("新增商品失败");
        }

        return Result.okOrFailed(commodityService.saveCommodity(commodity), "商品保存成功", "商品保存失败");
    }

    /**
     * 修改商品
     *
     * @param commodity 商品
     * @return 结果
     */
    @PostMapping("/edit_commodity")
    public Result<String> editCommodity(@RequestBody EditCommodityVO commodity) {
        if (commodityService.countCommodity(new CommodityQuery(commodity.getCommodityId())) <= 0) {
            return Result.failed("查无此商品,无法修改");
        }
        return Result.okOrFailed(commodityService.updateCommodity(commodity), "修改成功", "修改失败");
    }

    /**
     * 删除商品
     *
     * @param commodityId 商品标识
     * @return 结果
     */
    @GetMapping("/delete_commodity/{commodityId}")
    public Result<String> deleteCommodity(@PathVariable Long commodityId) {
        if (commodityService.countCommodity(new CommodityQuery(commodityId)) <= 0) {
            return Result.failed("查无此商品,无法删除");
        }

        return Result.okOrFailed(commodityService.deleteCommodity(commodityId), "删除成功", "删除失败");
    }


}
