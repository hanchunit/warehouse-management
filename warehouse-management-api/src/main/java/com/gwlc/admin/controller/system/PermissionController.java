package com.gwlc.admin.controller.system;

import com.gwlc.common.result.Result;
import com.gwlc.system.domain.dto.PermissionDTO;
import com.gwlc.system.domain.entity.UserDO;
import com.gwlc.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>warehouse-management-com.gwlc.controller-PermissionController</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/14
 */
@RestController
@RequestMapping("/permission")
public class PermissionController {
    @Autowired
    IUserService userService;

    @PostMapping("/get_list_user_permissions")
    public Result<List<PermissionDTO>> listUserPermissions(Long userId, String username) {
        if (userId == null && username == null) {
            return Result.failed("用户标识或用户名不能为空");
        }
        List<PermissionDTO> userPermissions = userService.listPermissions(new UserDO(userId, username));

        return Result.ok(userPermissions);
    }


}
