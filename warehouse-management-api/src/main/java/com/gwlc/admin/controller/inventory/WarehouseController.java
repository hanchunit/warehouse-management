package com.gwlc.admin.controller.inventory;

import com.gwlc.common.result.Result;
import com.gwlc.inventory.domain.dto.WarehouseDTO;
import com.gwlc.inventory.domain.entity.WarehouseDO;
import com.gwlc.inventory.domain.page.WarehousePage;
import com.gwlc.inventory.domain.query.WarehouseQuery;
import com.gwlc.inventory.domain.vo.SaveWarehouseCommodityVO;
import com.gwlc.inventory.domain.vo.SaveWarehouseVO;
import com.gwlc.inventory.service.WarehouseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * <p>warehouse-management-com.gwlc.admin.controller.inventory-WarehouseController</p>
 * <p>Description: 仓库 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/30
 */
@RequestMapping("/inventory/warehouse")
@RestController
public class WarehouseController {
    @Autowired
    WarehouseService warehouseService;

    /**
     * 获取仓库列表
     *
     * @param warehouse 仓库
     * @return 结果
     */
    @PostMapping("/get_list_warehouses")
    public Result<List<WarehouseDTO>> listCommodities(WarehouseDO warehouse) {


        return Result.ok(warehouseService.listWarehouses(new WarehouseQuery(warehouse)));
    }


    @PostMapping("/get_warehouse_page")
    public Result<WarehousePage> pageWarehouse(@RequestBody(required = false) WarehousePage warehouse) {
        if (warehouse == null) {
            warehouse = new WarehousePage();
        }
        warehouse.addLastLimit();
        System.out.println(warehouse);
        return Result.ok(warehouseService.pageWarehouse(warehouse));
    }

    /**
     * 精确获取仓库列表
     *
     * @param warehouseId 仓库标识
     * @return 结果
     */
    @GetMapping("/get_warehouse/{warehouseId}")
    public Result<WarehouseDTO> getWarehouseById(@PathVariable Serializable warehouseId) {
        return Result.ok(warehouseService.getWarehouse(warehouseId));
    }

    /**
     * 保存仓库
     *
     * @param warehouse 仓库信息
     * @return 结果
     */
    @PostMapping("/save_warehouse")
    public Result<String> saveWarehouse(@RequestBody SaveWarehouseVO warehouse) {


        if (warehouse.getWarehouseName() == null) {
            return Result.failed("仓库名不能为空");
        }

        return Result.okOrFailed(warehouseService.saveWarehouse(warehouse), "保存仓库成功", "保存仓库失败");

    }

    /**
     * 保存仓库商品信息
     *
     * @param saveWarehouseCommodity 仓库商品信息
     * @return 结果
     */
    @PostMapping("/save_warehouse_commodities")
    public Result<String> saveWarehouseCommodities(@RequestBody SaveWarehouseCommodityVO saveWarehouseCommodity) {
        System.out.println(saveWarehouseCommodity);
        return Result.okOrFailed(warehouseService.saveWarehouseCommodities(saveWarehouseCommodity), "仓库添加商品成功", "仓库添加商品失败");


    }

    /**
     * 修改仓库
     *
     * @param warehouse 仓库信息
     * @return 结果
     */
    @PostMapping("/edit_warehouse")
    public Result<String> editWarehouse(@RequestBody SaveWarehouseVO warehouse) {
        System.out.println(warehouse);
        return Result.okOrFailed(warehouseService.updateWarehouse(warehouse), "修改仓库成功", "修改仓库失败");

    }

    /**
     * 删除仓库
     *
     * @param warehouseId 仓库标识
     * @return 结果
     */
    @GetMapping("/delete_warehouse/{warehouseId}")
    public Result<String> deleteWarehouse(@PathVariable Long warehouseId) {
        return Result.okOrFailed(warehouseService.deleteWarehouse(warehouseId), "删除仓库成功", "删除仓库失败");
    }


}
