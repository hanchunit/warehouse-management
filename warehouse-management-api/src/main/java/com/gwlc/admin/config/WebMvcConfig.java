package com.gwlc.admin.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * <p>warehouse-management-com.dawn.config-WevMvcConfig</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(getCorsInterceptor()).addPathPatterns("/**").excludePathPatterns("/login", "/**/*.css", "/**/*.js", "/**/*.png", "/**/*.jpg", "/**/*.ico", "/**/*.jpeg", "/**/*.gif", "/**/fonts/*", "/base_exception/errors");
    }

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        //配置跨域
        registry.addMapping("/**");
    }

    @Bean
    public HandlerInterceptor getCorsInterceptor() {
        return new CorsInterceptor();
    }


}
