package com.gwlc.admin.config;


import com.gwlc.common.exception.base.BaseException;
import com.gwlc.common.result.Result;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * <p>warehouse-management-com.dawn.config-MyResponseBodyAdvice </p>
 * <p>Description:  全局异常捕获类(作用是异常信息统一格式输出) </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Component
@RestControllerAdvice

public class MyResponseBodyAdvice {

    public MyResponseBodyAdvice() {
    }


    /**
     * 全局自定义异常TestException捕获类
     *
     * @param baseException 基础异常
     * @return 异常消息
     */
    @ExceptionHandler(value = BaseException.class)
    public Result<String> userExceptionHandler(BaseException baseException) {

        return Result.exception(baseException);
    }


}