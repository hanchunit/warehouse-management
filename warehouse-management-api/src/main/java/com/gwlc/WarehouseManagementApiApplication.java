package com.gwlc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * <p>warehouse-management-com.gwlc-WarehouseManagementApiApplication</p>
 * <p>Description: 应用程序启动类 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/10
 */
@SpringBootApplication
public class WarehouseManagementApiApplication {
    public static void main(String[] args) {
        SpringApplication.run(WarehouseManagementApiApplication.class, args);
    }
}
