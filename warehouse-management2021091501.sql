/*
 Navicat Premium Data Transfer

 Source Server         : hanchun
 Source Server Type    : MySQL
 Source Server Version : 80023
 Source Host           : localhost:3306
 Source Schema         : warehouse-management

 Target Server Type    : MySQL
 Target Server Version : 80023
 File Encoding         : 65001

 Date: 15/09/2021 20:23:33
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for inventor_client
-- ----------------------------
DROP TABLE IF EXISTS `inventor_client`;
CREATE TABLE `inventor_client`
(
    `client_id`    bigint UNSIGNED                                         NOT NULL AUTO_INCREMENT COMMENT '客户标识',
    `client_name`  varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '默认名称' COMMENT '客户名称',
    `client_phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '手机号码',
    `create_time`  datetime                                                NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`  datetime                                                NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `remark`       varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
    PRIMARY KEY (`client_id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for inventory_classify
-- ----------------------------
DROP TABLE IF EXISTS `inventory_classify`;
CREATE TABLE `inventory_classify`
(
    `classify_id`     bigint UNSIGNED                                         NOT NULL AUTO_INCREMENT COMMENT '分类标识',
    `classify_name`   varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '分类名称',
    `parent_classify` bigint UNSIGNED                                         NULL DEFAULT 0 COMMENT '分类父标识',
    `classify_state`  tinyint UNSIGNED                                        NULL DEFAULT 0 COMMENT '分类状态',
    `create_by`       varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '创建者',
    `create_time`     datetime                                                NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_by`       varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '更新者',
    `update_time`     datetime                                                NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `remark`          varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
    PRIMARY KEY (`classify_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '分类表'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for inventory_commodity
-- ----------------------------
DROP TABLE IF EXISTS `inventory_commodity`;
CREATE TABLE `inventory_commodity`
(
    `commodity_id`            bigint UNSIGNED                                        NOT NULL AUTO_INCREMENT COMMENT '商品标识',
    `commodity_name`          varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品名',
    `commodity_brand`         varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '品牌',
    `commodity_specification` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '规格',
    `commodity_supplier`      varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '供应商',
    `create_time`             datetime                                               NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`             datetime                                               NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`commodity_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '商品表'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for inventory_commodity_classify
-- ----------------------------
DROP TABLE IF EXISTS `inventory_commodity_classify`;
CREATE TABLE `inventory_commodity_classify`
(
    `commodity_id` bigint UNSIGNED NOT NULL COMMENT '商品标识',
    `classify_id`  bigint UNSIGNED NOT NULL COMMENT '分类标识',
    `create_time`  datetime        NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`  datetime        NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`commodity_id`, `classify_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for inventory_delivery
-- ----------------------------
DROP TABLE IF EXISTS `inventory_delivery`;
CREATE TABLE `inventory_delivery`
(
    `delivery_id`    bigint UNSIGNED                                         NOT NULL AUTO_INCREMENT COMMENT '出库标识',
    `client_id`      bigint UNSIGNED                                         NULL DEFAULT NULL COMMENT '客户标识',
    `warehouse_id`   bigint                                                  NOT NULL COMMENT '仓库标识',
    `delivery_time`  datetime                                                NULL DEFAULT CURRENT_TIMESTAMP COMMENT '出库时间',
    `delivery_price` decimal(30, 10) UNSIGNED                                NULL DEFAULT NULL COMMENT '总价',
    `logistics`      varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '物流',
    `delivery_state` tinyint UNSIGNED                                        NULL DEFAULT 0 COMMENT '出库单状态',
    `create_time`    datetime                                                NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`    datetime                                                NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `remark`         varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
    PRIMARY KEY (`delivery_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '入库表'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for inventory_delivery_details
-- ----------------------------
DROP TABLE IF EXISTS `inventory_delivery_details`;
CREATE TABLE `inventory_delivery_details`
(
    `delivery_id`      bigint UNSIGNED                                         NOT NULL COMMENT '出库单标识',
    `commodity_id`     bigint UNSIGNED                                         NULL DEFAULT NULL COMMENT '商品标识',
    `commodity_price`  decimal(20, 10) UNSIGNED                                NULL DEFAULT NULL COMMENT '单价',
    `commodity_number` bigint UNSIGNED                                         NULL DEFAULT NULL COMMENT '数量',
    `commodity_prices` decimal(20, 10) UNSIGNED                                NULL DEFAULT NULL COMMENT '总价',
    `create_time`      datetime                                                NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`      datetime                                                NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `remark`           varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
    PRIMARY KEY (`delivery_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '入库详情'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for inventory_storage
-- ----------------------------
DROP TABLE IF EXISTS `inventory_storage`;
CREATE TABLE `inventory_storage`
(
    `storage_id`    bigint UNSIGNED                                         NOT NULL AUTO_INCREMENT COMMENT '入库标识',
    `client_id`     bigint UNSIGNED                                         NULL DEFAULT NULL COMMENT '客户标识',
    `warehouse_id`  bigint                                                  NOT NULL COMMENT '仓库标识',
    `storage_time`  datetime                                                NULL DEFAULT CURRENT_TIMESTAMP COMMENT '入库时间',
    `storage_price` decimal(30, 10) UNSIGNED                                NULL DEFAULT NULL COMMENT '总价',
    `logistics`     varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '物流',
    `storage_state` tinyint UNSIGNED                                        NULL DEFAULT 0 COMMENT '入库单状态',
    `create_time`   datetime                                                NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   datetime                                                NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `remark`        varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
    PRIMARY KEY (`storage_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '入库表'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for inventory_storage_details
-- ----------------------------
DROP TABLE IF EXISTS `inventory_storage_details`;
CREATE TABLE `inventory_storage_details`
(
    `storage_id`       bigint UNSIGNED                                         NOT NULL COMMENT '入库单标识',
    `commodity_id`     bigint UNSIGNED                                         NULL DEFAULT NULL COMMENT '商品标识',
    `commodity_price`  decimal(20, 10) UNSIGNED                                NULL DEFAULT NULL COMMENT '单价',
    `commodity_number` bigint UNSIGNED                                         NULL DEFAULT NULL COMMENT '数量',
    `commodity_prices` decimal(20, 10) UNSIGNED                                NULL DEFAULT NULL COMMENT '总价',
    `create_time`      datetime                                                NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`      datetime                                                NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `remark`           varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
    PRIMARY KEY (`storage_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '入库详情'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for inventory_warehouse
-- ----------------------------
DROP TABLE IF EXISTS `inventory_warehouse`;
CREATE TABLE `inventory_warehouse`
(
    `warehouse_id`    bigint UNSIGNED                                         NOT NULL AUTO_INCREMENT COMMENT '仓库标识',
    `warehouse_name`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '仓库名称',
    `warehouse_site`  varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '仓库地址',
    `warehouse_state` tinyint UNSIGNED                                        NULL DEFAULT 0 COMMENT '仓库状态',
    `create_by`       varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '创建者',
    `create_time`     datetime                                                NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_by`       varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '更新者',
    `update_time`     datetime                                                NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `remark`          varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
    PRIMARY KEY (`warehouse_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '仓库表'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for inventory_warehouse_commodity
-- ----------------------------
DROP TABLE IF EXISTS `inventory_warehouse_commodity`;
CREATE TABLE `inventory_warehouse_commodity`
(
    `warehouse_id`     bigint UNSIGNED NOT NULL COMMENT '仓库标识',
    `commodity_id`     bigint UNSIGNED NOT NULL COMMENT '商品标识',
    `commodity_number` bigint UNSIGNED NOT NULL DEFAULT 0 COMMENT '库存数量',
    `create_time`      datetime        NULL     DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`      datetime        NULL     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`warehouse_id`, `commodity_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci COMMENT = '仓库库存表'
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`
(
    `menu_id`       bigint UNSIGNED                                         NOT NULL AUTO_INCREMENT COMMENT '菜单标识',
    `menu_name`     varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '菜单名称',
    `parent_id`     bigint UNSIGNED                                         NULL DEFAULT 0 COMMENT '父菜单标识',
    `order_num`     int                                                     NULL DEFAULT 0 COMMENT '显示顺序',
    `url`           varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '请求地址',
    `target`        varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '打开方式（menuItem页签 menuBlank新窗口）',
    `menu_type`     char(1) CHARACTER SET utf8 COLLATE utf8_general_ci      NULL DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
    `menu_visible`  tinyint                                                 NULL DEFAULT 0 COMMENT '菜单状态（0显示 1隐藏）',
    `is_refresh`    tinyint                                                 NULL DEFAULT 1 COMMENT '是否刷新（0刷新 1不刷新）',
    `permission_id` bigint UNSIGNED                                         NULL DEFAULT NULL COMMENT '权限标识',
    `icon`          varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '#' COMMENT '菜单图标',
    `create_by`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '创建者',
    `create_time`   datetime                                                NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_by`     varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '更新者',
    `update_time`   datetime                                                NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `remark`        varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
    PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission`;
CREATE TABLE `sys_permission`
(
    `id`                 bigint UNSIGNED                                         NOT NULL AUTO_INCREMENT COMMENT '权限标识',
    `permission_name`    varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '权限名称',
    `permission_tag`     varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '权限标签',
    `permission_explain` varchar(12) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL DEFAULT '' COMMENT '权限说明',
    `create_by`          varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL     DEFAULT '' COMMENT '创建者',
    `create_time`        datetime                                                NULL     DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_by`          varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL     DEFAULT '' COMMENT '更新者',
    `update_time`        datetime                                                NULL     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `remark`             varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL     DEFAULT '' COMMENT '备注',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`
(
    `id`          int UNSIGNED                                            NOT NULL AUTO_INCREMENT COMMENT '角色标识',
    `role_name`   varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT NULL COMMENT '角色名称',
    `role_status` tinyint UNSIGNED                                        NULL DEFAULT 0 COMMENT '角色状态',
    `create_by`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '创建者',
    `create_time` datetime                                                NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_by`   varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci  NULL DEFAULT '' COMMENT '更新者',
    `update_time` datetime                                                NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `remark`      varchar(500) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '备注',
    `role_tag`    varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '角色标签',
    PRIMARY KEY (`id`) USING BTREE,
    UNIQUE INDEX `sys_role_role_tag_uindex` (`role_tag`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_role_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_permission`;
CREATE TABLE `sys_role_permission`
(
    `role_id`       bigint UNSIGNED NOT NULL COMMENT '角色标识',
    `permission_id` bigint UNSIGNED NOT NULL COMMENT '权限标识',
    `create_time`   datetime        NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   datetime        NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`role_id`, `permission_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`
(
    `user_id`       bigint UNSIGNED                                         NOT NULL AUTO_INCREMENT COMMENT '标识',
    `user_name`     varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '用户名',
    `user_password` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL COMMENT '密码',
    `user_nickname` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci  NOT NULL DEFAULT '未命名' COMMENT '昵称',
    `user_type`     tinyint UNSIGNED                                        NOT NULL DEFAULT 2 COMMENT '用户类型',
    `user_status`   tinyint UNSIGNED                                        NOT NULL DEFAULT 0 COMMENT '用户状态',
    `user_token`    varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL     DEFAULT NULL COMMENT '用户登录密钥',
    `create_time`   datetime                                                NULL     DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   datetime                                                NULL     DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 1
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user_permission
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_permission`;
CREATE TABLE `sys_user_permission`
(
    `user_id`       bigint UNSIGNED NOT NULL COMMENT '用户标识',
    `permission_id` bigint UNSIGNED NOT NULL COMMENT '权限标识',
    `create_time`   datetime        NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`   datetime        NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`user_id`, `permission_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`
(
    `user_id`     bigint UNSIGNED NOT NULL COMMENT '用户标识',
    `role_id`     bigint UNSIGNED NOT NULL COMMENT '角色标识',
    `create_time` datetime        NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time` datetime        NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    PRIMARY KEY (`user_id`, `role_id`) USING BTREE
) ENGINE = InnoDB
  CHARACTER SET = utf8
  COLLATE = utf8_general_ci
  ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
