package com.gwlc.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

import java.util.Date;

/**
 * <p>warehouse-management-com.gwlc.system.domain-User</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/3
 */

@Data
@TableName("sys_user")
public class UserDO extends BaseSerializableObject {
    private static final long serialVersionUID = -1405654983557425826L;
    /**
     * 创建时间
     */
    protected Date createTime;
    /**
     * 用户标识
     */
    @TableId
    private Long userId;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 登录密码
     */
    private String userPassword;
    /**
     * 用户昵称
     */
    private String userNickname;
    /**
     * 用户类型
     */
    private Integer userType;
    /**
     * 用户状态
     */
    private Integer userStatus;
    /**
     * 用户token
     */
    private String userToken;
    /**
     * 更新时间
     */
    private Date updateTime;

    public UserDO() {
    }

    public UserDO(Long userId, String userName) {
        this.userId = userId;
        this.userName = userName;
    }
}
