package com.gwlc.system.domain.dto;

import com.fasterxml.jackson.annotation.JsonValue;
import com.gwlc.common.core.Tree;
import com.gwlc.system.domain.entity.MenuDO;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>warehouse-management-com.gwlc.system.domain-MenuTree</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/18
 */
@Data
public class MenuTree extends Tree {
    private static final long serialVersionUID = -6194089310336936682L;


    /**
     * 菜单排序
     */
    private Integer orderNumber;
    /**
     * 路径
     */
    private String url;
    /**
     * 打开方式
     */
    private String target;
    /**
     * 菜单类型
     */
    private String menuType;
    /**
     * 菜单状态
     */
    private Integer visible;
    /**
     * 是否刷新
     */
    private Boolean refresh;
    /**
     * 权限标识
     */
    private Long permissionId;
    /**
     * 菜单图标
     */
    private String icon;

    public MenuTree() {
    }

    public MenuTree(Long treeId, Long parentId, String treeName, List<Tree> subtree, Integer orderNumber, String url, String target, String menuType, Integer visible, Boolean refresh, Long permissionId, String icon) {
        super(treeId, parentId, treeName, subtree);
        this.orderNumber = orderNumber;
        this.url = url;
        this.target = target;
        this.menuType = menuType;
        this.visible = visible;
        this.refresh = refresh;
        this.permissionId = permissionId;
        this.icon = icon;
    }

    public MenuTree(MenuDO menu) {
        super(menu.getMenuId(), menu.getParentId(), menu.getMenuName(), null);
        this.orderNumber = menu.getOrderNum();
        this.url = menu.getUrl();
        this.target = menu.getTarget();
        this.menuType = menu.getMenuType();
        this.visible = menu.getMenuVisible();
        this.refresh = menu.getIsRefresh() == 0;
        this.permissionId = menu.getPermissionId();
        this.icon = menu.getIcon();

    }

    @Override
    public String toString() {
        return "MenuTree{" +
                "orderNumber='" + orderNumber + '\'' +
                ", url='" + url + '\'' +
                ", target='" + target + '\'' +
                ", menuType='" + menuType + '\'' +
                ", visible='" + visible + '\'' +
                ", refresh=" + refresh +
                ", permissionId=" + permissionId +
                ", icon='" + icon + '\'' +
                "} " + super.toString();
    }

    @JsonValue
    public Map<String, Object> toJson() {
        Map<String, Object> menuMap = new HashMap<>(16);

        menuMap.put("title", getName());
        menuMap.put("id", getId());
        menuMap.put("field", getId());
        menuMap.put("children", getSubtree());
        menuMap.put("href", getUrl());
        menuMap.put("spread", false);
        menuMap.put("checked", false);
        menuMap.put("disabled", false);
        return menuMap;

    }


}
