package com.gwlc.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gwlc.common.base.BaseEntity;
import lombok.Data;

/**
 * @author hanchun
 * @TableName sys_menu
 */
@TableName(value = "sys_menu")
@Data
public class MenuDO extends BaseEntity {
    private static final long serialVersionUID = -686412026564911211L;
    /**
     * 菜单标识
     */
    @TableId(value = "menu_id", type = IdType.AUTO)
    private Long menuId;

    /**
     * 菜单名称
     */
    @TableField(value = "menu_name")
    private String menuName;

    /**
     * 父菜单标识
     */
    @TableField(value = "parent_id")
    private Long parentId;

    /**
     * 显示顺序
     */
    @TableField(value = "order_num")
    private Integer orderNum;

    /**
     * 请求地址
     */
    @TableField(value = "url")
    private String url;

    /**
     * 打开方式（menuItem页签 menuBlank新窗口）
     */
    @TableField(value = "target")
    private String target;

    /**
     * 菜单类型（M目录 C菜单 F按钮）
     */
    @TableField(value = "menu_type")
    private String menuType;

    /**
     * 菜单状态（0显示 1隐藏）
     */
    @TableField(value = "menu_visible")
    private Integer menuVisible;

    /**
     * 是否刷新（0刷新 1不刷新）
     */
    @TableField(value = "is_refresh")
    private Integer isRefresh;

    /**
     * 权限标识
     */
    @TableField(value = "permission_id")
    private Long permissionId;

    /**
     * 菜单图标
     */
    @TableField(value = "icon")
    private String icon;

}