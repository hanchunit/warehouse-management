package com.gwlc.system.domain.dto;

import com.fasterxml.jackson.annotation.JsonValue;
import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>warehouse-management-com.gwlc.system.domain.dto-UserRoleDTO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/28
 */
@Data
public class UserRoleDTO extends BaseSerializableObject {
    private static final long serialVersionUID = -8703307147078072006L;

    /**
     * 用户标识
     */
    private Long userId;
    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户token
     */
    private String userToken;
    /**
     * 用户角色
     */
    private Map<String, String> role;

    public void setUserRole(Long userId, String userName, String userToken, List<RoleDTO> roles) {
        this.userId = userId;
        this.userName = userName;
        this.userToken = userToken;

        if (!roles.isEmpty()) {
            Map<String, String> map = new HashMap<>();
            for (RoleDTO role : roles) {
                if (role != null) {
                    map.put(role.getRoleName(), role.getRoleTag());
                }
            }
            this.role = map;
        } else {
            this.role = new HashMap<>();
        }
    }

    /**
     * 用户map
     *
     * @return 用户map
     */
    @JsonValue
    public Map<String, Object> toJson() {

        Map<String, Object> map = new HashMap<>();
        map.put("userId", getUserId());
        map.put("userName", getUserName());
        map.put("userToken", getUserToken());
        map.put("roles", getRole());
        return map;
    }
}
