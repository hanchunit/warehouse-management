package com.gwlc.system.domain.dto;

import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

/**
 * <p>warehouse-management-com.gwlc.system.domain.dto-RoleDTO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/28
 */
@Data
public class RoleDTO extends BaseSerializableObject {
    private static final long serialVersionUID = 1951591443051032080L;

    /**
     * 角色标识
     */
    private Integer id;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 角色状态
     */
    private Integer roleStatus;

    /**
     * 权限标签
     */
    private String roleTag;
}
