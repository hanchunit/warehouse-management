package com.gwlc.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gwlc.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * @author hanchun
 * @TableName sys_permission
 */
@TableName(value = "sys_permission")
@Data
public class PermissionDO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 5059623002447047748L;
    /**
     * 权限标识
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 权限名称
     */
    @TableField(value = "permission_name")
    private String permissionName;

    /**
     * 权限标签
     */
    @TableField(value = "permission_tag")
    private String permissionTag;

    /**
     * 权限说明
     */
    @TableField(value = "permission_explain")
    private String permissionExplain;


}