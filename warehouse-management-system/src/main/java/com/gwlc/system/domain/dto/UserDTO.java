package com.gwlc.system.domain.dto;

import com.fasterxml.jackson.annotation.JsonValue;
import com.gwlc.common.base.BaseSerializableObject;
import com.gwlc.common.enums.system.user.UserStatusEnum;
import com.gwlc.common.enums.system.user.UserTypeEnum;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>warehouse-management-com.gwlc.system.domain.dto-UserDTO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/4
 */
@Data
public class UserDTO extends BaseSerializableObject {
    private static final long serialVersionUID = -5287606593443276571L;

    /**
     * 用户标识
     */
    private Long userId;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 用户昵称
     */
    private String userNickname;
    /**
     * 用户类型
     */
    private UserTypeEnum userType;
    /**
     * 用户状态
     */
    private UserStatusEnum userStatus;
    /**
     * 用户token
     */
    private String userToken;

    public void setUserType(Integer userType) {
        this.userType = UserTypeEnum.valueOf(userType);
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = UserStatusEnum.valueOf(userStatus);
    }


    @JsonValue
    public Map<String, Object> toJson() {
        Map<String, Object> jsonMap = new HashMap<>(16);

        jsonMap.put("userId", getUserId());
        jsonMap.put("userName", getUserName());
        jsonMap.put("userNickname", getUserNickname());
        jsonMap.put("userType", getUserType().toString());
        jsonMap.put("userStatus", getUserStatus().toString());
        jsonMap.put("userToken", getUserToken());

        return jsonMap;
    }
}
