package com.gwlc.system.domain.dto;

import com.fasterxml.jackson.annotation.JsonValue;
import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>warehouse-management-com.gwlc.system.domain.DTO-UserPermissionDTO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/21
 */
@Data
public class UserPermissionDTO extends BaseSerializableObject {
    private static final long serialVersionUID = 5725604456538002738L;

    /**
     * 用户标识
     */
    private Long userId;
    /**
     * 用户名
     */
    private String userName;

    /**
     * 用户token
     */
    private String userToken;

    private Map<String, String> permissions;


    public void setUserPermission(Long userId, String userName, String userToken, List<PermissionDTO> permissions) {
        this.userId = userId;
        this.userName = userName;
        this.userToken = userToken;
        System.out.println(permissions);
        if (!permissions.isEmpty()) {
            Map<String, String> map = new HashMap<>();
            for (PermissionDTO permission : permissions) {
                if (permission != null) {
                    map.put(permission.getPermissionName(), permission.getPermissionTag());
                }
            }
            this.permissions = map;
        } else {
            this.permissions = new HashMap<>();
        }
    }

    /**
     * 用户map
     *
     * @return 用户map
     */
    @JsonValue
    public Map<String, Object> toJson() {

        Map<String, Object> map = new HashMap<>();
        map.put("userId", getUserId());
        map.put("userName", getUserName());
        map.put("userToken", getUserToken());
        map.put("permissions", getPermissions());
        return map;
    }
}
