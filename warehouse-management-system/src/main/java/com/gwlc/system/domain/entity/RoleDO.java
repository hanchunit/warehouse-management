package com.gwlc.system.domain.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.gwlc.common.base.BaseEntity;
import lombok.Data;

import java.io.Serializable;

/**
 * @TableName sys_role
 */
@TableName(value = "sys_role")
@Data
public class RoleDO extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1093053223637157426L;
    /**
     * 角色标识
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 角色名称
     */
    @TableField(value = "role_name")
    private String roleName;

    /**
     * 角色状态
     */
    @TableField(value = "role_status")
    private Integer roleStatus;

    /**
     * 权限标签
     */
    @TableField(value = "role_tag")
    private String roleTag;

}