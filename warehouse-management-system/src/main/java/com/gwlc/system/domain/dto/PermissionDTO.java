package com.gwlc.system.domain.dto;

import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;

/**
 * <p>warehouse-management-com.gwlc.system.domain.DTO-PermissionDTO</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/21
 */
@Data
public class PermissionDTO extends BaseSerializableObject {
    private static final long serialVersionUID = 4128161179886423287L;
    /**
     * 权限标识
     */
    private Long permissionId;

    /**
     * 权限名称
     */
    private String permissionName;

    /**
     * 权限标签
     */
    private String permissionTag;

    /**
     * 权限说明
     */
    private String permissionExplain;

}
