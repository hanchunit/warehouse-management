package com.gwlc.system.domain.query;

import com.gwlc.common.base.BaseSerializableObject;
import com.gwlc.system.domain.entity.MenuDO;
import lombok.Data;

/**
 * <p>warehouse-management-com.gwlc.system.domain.query-MenuQuery</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/7
 */
@Data
public class MenuQuery extends BaseSerializableObject {
    private static final long serialVersionUID = -1140476390207735897L;

    /**
     * 菜单标识
     */
    private Long menuId;

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 父菜单标识
     */
    private Long parentId;

    /**
     * 显示顺序
     */
    private Integer orderNum;

    /**
     * 请求地址
     */
    private String url;

    /**
     * 打开方式（menuItem页签 menuBlank新窗口）
     */
    private String target;

    /**
     * 菜单类型（M目录 C菜单 F按钮）
     */
    private String menuType;

    /**
     * 菜单状态（0显示 1隐藏）
     */
    private Integer menuVisible;

    /**
     * 是否刷新（0刷新 1不刷新）
     */
    private Integer isRefresh;

    /**
     * 权限标识
     */
    private Long permissionId;

    /**
     * 菜单图标
     */
    private String icon;

    public MenuQuery(MenuDO menu) {
        if (menu != null) {
            this.menuId = menu.getMenuId();
            this.menuName = menu.getMenuName();
            this.parentId = menu.getParentId();
            this.orderNum = menu.getOrderNum();
            this.url = menu.getUrl();
            this.target = menu.getTarget();
            this.menuType = menu.getMenuType();
            this.menuVisible = menu.getMenuVisible();
            this.isRefresh = menu.getIsRefresh();
            this.permissionId = menu.getPermissionId();
            this.icon = menu.getIcon();
        }
    }

}
