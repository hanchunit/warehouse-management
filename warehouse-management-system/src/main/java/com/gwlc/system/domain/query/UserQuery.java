package com.gwlc.system.domain.query;

import com.gwlc.common.base.BaseSerializableObject;
import com.gwlc.system.domain.dto.UserDTO;
import com.gwlc.system.domain.entity.UserDO;
import lombok.Data;

/**
 * <p>warehouse-management-com.gwlc.system.domain.query-UserQuery</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/3
 */
@Data
public class UserQuery extends BaseSerializableObject {
    private static final long serialVersionUID = 2664364076932071606L;
    /**
     * 用户标识
     */
    private Long userId;
    /**
     * 用户名
     */
    private String userName;
    /**
     * 登录密码
     */
    private String userPassword;
    /**
     * 用户昵称
     */
    private String userNickname;
    /**
     * 用户类型
     */
    private Integer userType;
    /**
     * 用户状态
     */
    private Integer userStatus;
    /**
     * 用户token
     */
    private String userToken;

    public UserQuery() {
    }

    /**
     * 根据用户创建用户查询条件
     *
     * @param user 用户信息
     */
    public UserQuery(UserDO user) {

        if (user != null) {
            this.userId = user.getUserId();
            this.userName = user.getUserName();
            this.userPassword = user.getUserPassword();
            this.userNickname = user.getUserNickname();
            this.userType = user.getUserType();
            this.userStatus = user.getUserStatus();
            this.userToken = user.getUserToken();
        }
    }

    public UserQuery(UserDTO user) {
        if (user != null) {
            this.userId = user.getUserId();
            this.userName = user.getUserName();
            this.userNickname = user.getUserNickname();
            this.userType = user.getUserType().getValue();
            this.userStatus = user.getUserStatus().getValue();
            this.userToken = user.getUserToken();
        }
    }


}
