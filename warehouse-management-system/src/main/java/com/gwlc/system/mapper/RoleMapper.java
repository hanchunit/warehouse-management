package com.gwlc.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gwlc.system.domain.entity.RoleDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.gwlc.system.domain.entity.RoleDO
 */
@Mapper
public interface RoleMapper extends BaseMapper<RoleDO> {

}




