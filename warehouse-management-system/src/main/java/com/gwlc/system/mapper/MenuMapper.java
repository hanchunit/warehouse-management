package com.gwlc.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gwlc.system.domain.entity.MenuDO;
import com.gwlc.system.domain.query.MenuQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>warehouse-management-com.gwlc.system.mapper-MenuMapper</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/18
 */
@Mapper
public interface MenuMapper extends BaseMapper<MenuDO> {
    /**
     * 菜单列表
     *
     * @param menu 菜单信息
     * @return 菜单列表
     */
    List<MenuDO> listMenus(MenuQuery menu);
}
