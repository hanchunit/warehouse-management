package com.gwlc.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gwlc.system.domain.dto.PermissionDTO;
import com.gwlc.system.domain.dto.RoleDTO;
import com.gwlc.system.domain.entity.UserDO;
import com.gwlc.system.domain.query.UserQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>team-management-system-com.dawn.system.mapper-UserMapper</p>
 * <p>Description: 用户映射 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/7/28
 */
@Mapper
public interface UserMapper extends BaseMapper<UserDO> {
    /**
     * 查询用户
     *
     * @param userId 用户ID
     * @return 用户
     */
    UserDO getUserById(Long userId);

    /**
     * 查询用户列表
     *
     * @param user 用户
     * @return 用户集合
     */
    List<UserDO> listUsers(UserQuery user);

    /**
     * 查询用户
     *
     * @param user 用户
     * @return 用户
     */
    UserDO getUser(UserQuery user);

    /**
     * 新增用户
     *
     * @param user 用户
     * @return 结果
     */
    int insertUser(UserQuery user);

    /**
     * 修改用户
     *
     * @param user 用户
     * @return 结果
     */
    int updateUser(UserQuery user);

    /**
     * 删除用户
     *
     * @param userId 用户ID
     * @return 结果
     */
    int deleteUserById(Long userId);

    /**
     * 批量删除用户
     *
     * @param userIds 需要删除的数据ID
     * @return 结果
     */
    int deleteUserByIds(String[] userIds);

    /**
     * 统计用户
     *
     * @param user 用户
     * @return 结果
     */
    int countUser(UserQuery user);


    /**
     * 获取用户权限
     *
     * @param user 用户
     * @return 用户权限
     */
    List<PermissionDTO> listPermissions(UserQuery user);

    /**
     * 获取用户角色
     *
     * @param user 用户
     * @return 用户角色
     */
    List<RoleDTO> listRoles(UserQuery user);

}
