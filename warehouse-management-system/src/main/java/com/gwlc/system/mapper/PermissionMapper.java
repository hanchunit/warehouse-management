package com.gwlc.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.gwlc.system.domain.entity.PermissionDO;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Entity com.gwlc.system.domain.entity.PermissionDO
 */
@Mapper
public interface PermissionMapper extends BaseMapper<PermissionDO> {

}




