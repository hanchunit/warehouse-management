package com.gwlc.system.aop;


import com.gwlc.common.annotation.RoleAnnotation;
import com.gwlc.common.base.BaseAop;
import com.gwlc.common.constant.system.user.UserStateConstant;
import com.gwlc.common.constant.system.user.UserTypeConstant;
import com.gwlc.common.exception.user.*;
import com.gwlc.common.util.TokenTool;
import com.gwlc.system.domain.entity.UserDO;
import com.gwlc.system.service.IUserService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Arrays;

/**
 * <p>warehouse-management-com.gwlc.system.aop-PermissionVerifyAop</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/21
 */
@Component
@Order(1)
@Aspect
public class RoleVerifyAop extends BaseAop {

    @Autowired
    private IUserService userService;

    /**
     * 定义切点
     */
    @Pointcut("@annotation(com.gwlc.common.annotation.RoleAnnotation)")
    public void roleAop() {
    }

    /**
     * 权限环绕通知
     *
     * @param point 提供对连接点可用状态和有关它的静态信息的反射访问
     */
    @Before("roleAop()")
    @ResponseBody
    public void isAccessMethod(JoinPoint point) {



        MethodSignature signature = (MethodSignature) point.getSignature();
        RoleAnnotation roleAnnotation = signature.getMethod().getDeclaredAnnotation(RoleAnnotation.class);

        String[] roleTags = roleAnnotation.value();

        System.out.println("****************************************");
        System.out.println("需要的角色" + Arrays.toString(roleTags));
        System.out.println("****************************************");
        String token = getToken();

        if (token == null || "".equals(token)) {
            throw new NotLoginException();
        }
        //获取令牌超时时间
        Long outTime = null;
        try {
            outTime = (Long) (TokenTool.decodeTokenMap(token)).get("outTime");
        } catch (Exception e) {
            throw new NotLoginException();
        }

        if (verifyOutTime(outTime)) {
            throw new TokenOutTimeException();
        }

        UserDO user = userService.getUserByToken(token);

        if (user.getUserToken().isEmpty()) {
            throw new NotTokenException();
        }

        if (!user.getUserStatus().equals(UserStateConstant.USER_STATE_NORMAL)) {
            throw new UserStateException();
        }

        if (user.getUserType().equals(UserTypeConstant.USER_TYPE_ROOT)) {
            return;
        }

        if (roleTags.length <= 0) {
            return;
        }


        throw new NotPermissionException();
    }


}
