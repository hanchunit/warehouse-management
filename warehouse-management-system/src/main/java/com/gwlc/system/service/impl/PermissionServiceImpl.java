package com.gwlc.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gwlc.system.domain.entity.PermissionDO;
import com.gwlc.system.mapper.PermissionMapper;
import com.gwlc.system.service.IPermissionService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, PermissionDO>
        implements IPermissionService {

}




