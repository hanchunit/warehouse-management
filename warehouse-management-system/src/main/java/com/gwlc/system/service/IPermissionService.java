package com.gwlc.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gwlc.system.domain.entity.PermissionDO;

/**
 * @author hanchun
 */
public interface IPermissionService extends IService<PermissionDO> {

}
