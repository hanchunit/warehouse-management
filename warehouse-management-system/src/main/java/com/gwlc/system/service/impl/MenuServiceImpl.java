package com.gwlc.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gwlc.common.util.TreeTool;
import com.gwlc.system.domain.dto.MenuTree;
import com.gwlc.system.domain.entity.MenuDO;
import com.gwlc.system.domain.query.MenuQuery;
import com.gwlc.system.mapper.MenuMapper;
import com.gwlc.system.service.IMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, MenuDO>
        implements IMenuService {

    @Autowired
    MenuMapper menuMapper;

    /**
     * 获取所有菜单
     *
     * @param menuQuery 菜单信息
     * @return 所有菜单
     */
    @Override
    public List<MenuTree> listMenus(MenuQuery menuQuery) {
        List<MenuDO> menus = menuMapper.listMenus(menuQuery);
        List<MenuTree> menuTrees = new ArrayList<>();
        if (menus.isEmpty()) {
            return menuTrees;
        }
        for (MenuDO menu : menus) {
            menuTrees.add(new MenuTree(menu));
        }
        return TreeTool.builderTree(menuTrees);
    }
}




