package com.gwlc.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gwlc.system.domain.dto.MenuTree;
import com.gwlc.system.domain.entity.MenuDO;
import com.gwlc.system.domain.query.MenuQuery;

import java.util.List;

/**
 * <p>warehouse-management-com.gwlc.system.service-IMenuService</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/18
 */

public interface IMenuService extends IService<MenuDO> {
    /**
     * 获取所有菜单
     *
     * @param menuQuery 菜单信息
     * @return 所有菜单
     */
    List<MenuTree> listMenus(MenuQuery menuQuery);
}
