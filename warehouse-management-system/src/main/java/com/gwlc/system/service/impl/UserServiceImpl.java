package com.gwlc.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gwlc.common.enums.system.user.UserStatusEnum;
import com.gwlc.common.enums.system.user.UserTypeEnum;
import com.gwlc.system.domain.dto.PermissionDTO;
import com.gwlc.system.domain.dto.UserDTO;
import com.gwlc.system.domain.dto.UserPermissionDTO;
import com.gwlc.system.domain.dto.UserRoleDTO;
import com.gwlc.system.domain.entity.UserDO;
import com.gwlc.system.domain.query.UserQuery;
import com.gwlc.system.mapper.UserMapper;
import com.gwlc.system.service.IUserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * <p>warehouse-management-com.gwlc.system.service.impl-UserServiceImpl</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/3
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, UserDO> implements IUserService {
    @Autowired
    UserMapper userMapper;

    UserQuery userQuery;

    /**
     * 根据条件获取用户信息
     *
     * @param user 用户条件
     * @return 符合条件的用户
     */
    @Override
    public List<UserDTO> listUsers(UserQuery user) {
        if (user == null) {
            user = new UserQuery();
        }
        List<UserDO> users = userMapper.listUsers(user);


        List<UserDTO> userResults = new ArrayList<>();
        if (users.isEmpty()) {
            return userResults;
        }
        UserDTO userDTO;
        for (UserDO userDO : users) {
            userDTO = new UserDTO();
            BeanUtils.copyProperties(userDO, userDTO);
            userResults.add(userDTO);
        }
        return userResults;
    }

    /**
     * 获取单个用户信息
     *
     * @param user 用户
     * @return 用户信息
     */
    @Override
    public UserDTO getUser(UserQuery user) {
        UserDO userDO = userMapper.getUser(user);
        UserDTO userDTO = new UserDTO();
        if (userDO == null) {
            return userDTO;
        } else {
            BeanUtils.copyProperties(userDO, userDTO);
            return userDTO;
        }


    }

    /**
     * 验证用户名是否存在
     *
     * @param userName 用户名
     * @return 存在则返回true, 不存在返回false
     */
    @Override
    public Boolean verifyUsername(String userName) {

        userQuery = new UserQuery();
        userQuery.setUserName(userName);
        return userMapper.countUser(userQuery) > 0;
    }

    /**
     * 验证用户名及密码是否对应
     *
     * @param userName     用户名
     * @param userPassword 密码
     * @return 对应则返回true, 不对应返回false
     */
    @Override
    public Boolean verifyUsernameAndPassword(String userName, String userPassword) {

        userQuery = new UserQuery();
        userQuery.setUserName(userName);
        userQuery.setUserPassword(userPassword);
        return userMapper.countUser(userQuery) > 0;
    }

    /**
     * 验证token是否有效
     *
     * @param token 用户令牌
     * @return 有效返回true, 失效返回false
     */
    @Override
    public Boolean verifyToken(String token) {

        userQuery = new UserQuery();
        userQuery.setUserToken(token);
        return userMapper.countUser(userQuery) > 0;

    }

    /**
     * 根据用户名获取用户状态
     *
     * @param userName 用户名
     * @return 用户状态
     */
    @Override
    public UserStatusEnum getUserStatusByUsername(String userName) {

        userQuery = new UserQuery();
        userQuery.setUserName(userName);
        UserDO user = userMapper.getUser(userQuery);
        if (user == null) {
            return null;
        } else {
            return UserStatusEnum.valueOf(user.getUserStatus());
        }


    }

    /**
     * 根据用户名获取用户类型
     *
     * @param userName 用户名
     * @return 用户类型
     */
    @Override
    public UserTypeEnum getUserTypeByUsername(String userName) {

        userQuery = new UserQuery();
        userQuery.setUserName(userName);
        UserDO user = userMapper.getUser(userQuery);
        if (user == null) {
            return null;
        } else {
            return UserTypeEnum.valueOf(user.getUserType());
        }
    }

    /**
     * 获取用户类型
     *
     * @param token 用户类型
     * @return 用户类型
     */
    @Override
    public UserTypeEnum getUserTypeByToken(String token) {

        userQuery = new UserQuery();
        userQuery.setUserToken(token);
        UserDO user = userMapper.getUser(userQuery);
        if (user == null) {
            return null;
        } else {
            return UserTypeEnum.valueOf(user.getUserType());
        }
    }

    /**
     * 设置用户令牌
     *
     * @param userName     用户名
     * @param userPassword 密码
     * @param token        用户令牌
     * @return 设置成功返回true, 失败返回false
     */
    @Override
    public Boolean setUserToken(String userName, String userPassword, String token) {
        userQuery = new UserQuery();
        userQuery.setUserName(userName);
        userQuery.setUserPassword(userPassword);
        UserDO user = userMapper.getUser(userQuery);
        if (user == null || user.getUserId() <= 0) {
            return null;
        }
        userQuery = new UserQuery();
        userQuery.setUserId(user.getUserId());
        userQuery.setUserToken(token);

        return userMapper.updateUser(userQuery) > 0;
    }

    /**
     * 保存用户
     *
     * @param user 用户
     * @return 是否保存成功
     */
    @Override
    public Boolean saveUser(UserQuery user) {
        return userMapper.insertUser(user) > 0;
    }

    /**
     * 更新用户
     *
     * @param user 用户
     * @return 是否更新成功
     */
    @Override
    public Boolean updateUser(UserQuery user) {
        return userMapper.updateUser(user) > 0;
    }

    /**
     * 权限集合
     *
     * @param user 用户
     * @return 权限集合
     */
    @Override
    public List<PermissionDTO> listPermissions(UserDO user) {
        return getBaseMapper().listPermissions(new UserQuery(user));
    }

    /**
     * 获取用户及权限
     *
     * @param user 用户
     * @return 用户及权限
     */
    @Override
    public UserPermissionDTO getUserPermission(UserDO user) {

        UserDO userDO = getBaseMapper().getUser(new UserQuery(user));
        UserPermissionDTO userPermission = new UserPermissionDTO();
        if (userDO.getUserId() != null) {
            userPermission.setUserPermission(userDO.getUserId(), userDO.getUserName(), userDO.getUserToken(), getBaseMapper().listPermissions(new UserQuery(user)));
        }
        return userPermission;
    }

    /**
     * 获取用户及角色
     *
     * @param user 用户
     * @return 用户及角色
     */
    @Override
    public UserRoleDTO getUserRole(UserDO user) {

        UserDO userDO = getBaseMapper().getUser(new UserQuery(user));
        UserRoleDTO userRole = new UserRoleDTO();
        if (userDO.getUserId() != null) {
            userRole.setUserRole(userDO.getUserId(), userDO.getUserName(), userDO.getUserToken(), getBaseMapper().listRoles(new UserQuery(user)));
        }
        return userRole;
    }

    /**
     * 根据token获取用户
     *
     * @param token 用户令牌
     * @return 用户
     */
    @Override
    public UserDO getUserByToken(String token) {
        userQuery = new UserQuery();
        userQuery.setUserToken(token);
        return getBaseMapper().getUser(userQuery);
    }


}

