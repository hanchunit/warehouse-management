package com.gwlc.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gwlc.common.enums.system.user.UserStatusEnum;
import com.gwlc.common.enums.system.user.UserTypeEnum;
import com.gwlc.system.domain.dto.PermissionDTO;
import com.gwlc.system.domain.dto.UserDTO;
import com.gwlc.system.domain.dto.UserPermissionDTO;
import com.gwlc.system.domain.dto.UserRoleDTO;
import com.gwlc.system.domain.entity.UserDO;
import com.gwlc.system.domain.query.UserQuery;

import java.util.List;

/**
 * <p>warehouse-management-com.gwlc.system.service-IUserService</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/3
 */
public interface IUserService extends IService<UserDO> {
    /**
     * 根据条件获取用户信息
     *
     * @param user 用户条件
     * @return 符合条件的用户
     */
    List<UserDTO> listUsers(UserQuery user);

    /**
     * 获取单个用户信息
     *
     * @param user 用户
     * @return 用户信息
     */
    UserDTO getUser(UserQuery user);

    /**
     * 验证用户名是否存在
     *
     * @param userName 用户名
     * @return 存在则返回true, 不存在返回false
     */
    Boolean verifyUsername(String userName);

    /**
     * 验证用户名及密码是否对应
     *
     * @param userName     用户名
     * @param userPassword 密码
     * @return 对应则返回true, 不对应返回false
     */
    Boolean verifyUsernameAndPassword(String userName, String userPassword);

    /**
     * 验证token是否有效
     *
     * @param token 用户令牌
     * @return 有效返回true, 失效返回false
     */
    Boolean verifyToken(String token);

    /**
     * 根据用户名获取用户状态
     *
     * @param userName 用户名
     * @return 用户状态
     */
    UserStatusEnum getUserStatusByUsername(String userName);


    /**
     * 根据用户名获取用户类型
     *
     * @param userName 用户名
     * @return 用户类型
     */
    UserTypeEnum getUserTypeByUsername(String userName);


    /**
     * 获取用户类型
     *
     * @param token 用户令牌
     * @return 用户类型
     */
    UserTypeEnum getUserTypeByToken(String token);

    /**
     * 设置用户令牌
     *
     * @param userName     用户名
     * @param userPassword 密码
     * @param token        用户令牌
     * @return 设置成功返回true, 失败返回false
     */
    Boolean setUserToken(String userName, String userPassword, String token);

    /**
     * 保存用户
     *
     * @param user 用户
     * @return 是否保存成功
     */
    Boolean saveUser(UserQuery user);

    /**
     * 更新用户
     *
     * @param user 用户
     * @return 是否更新成功
     */
    Boolean updateUser(UserQuery user);

    /**
     * 权限集合
     *
     * @param user 用户
     * @return 权限集合
     */
    List<PermissionDTO> listPermissions(UserDO user);

    /**
     * 获取用户及权限
     *
     * @param user 用户
     * @return 用户及权限
     */
    UserPermissionDTO getUserPermission(UserDO user);

    /**
     * 获取用户及角色
     *
     * @param user 用户
     * @return 用户及角色
     */
    UserRoleDTO getUserRole(UserDO user);

    /**
     * 根据token获取用户
     *
     * @param token 用户令牌
     * @return 用户
     */
    UserDO getUserByToken(String token);
}
