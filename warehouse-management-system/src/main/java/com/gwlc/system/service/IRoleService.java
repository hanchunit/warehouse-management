package com.gwlc.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.gwlc.system.domain.entity.RoleDO;

/**
 * @author hanchun
 */
public interface IRoleService extends IService<RoleDO> {

}
