package com.gwlc.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.gwlc.system.domain.entity.RoleDO;
import com.gwlc.system.mapper.RoleMapper;
import com.gwlc.system.service.IRoleService;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, RoleDO>
        implements IRoleService {

}




