# WarehouseManagement

#### 介绍
对仓库和商品的出入库进行管理,细化了权限及角色认证

#### 软件架构
- 后端采用spring boot+mybatis-plus搭建
- 前端采用spring boot+layui搭建
- 数据库采用mysql8.0
- 前端采用Cooick存储用户令牌,后端采用切面+拦截器对用户角色及权限进行验证
- 采用maven进行分模块构建,可扩展自定义模块

#### 安装教程

1. 导入数据库:warehouse-management.sql
2. 修改api及admin模块中yml文件的数据源配置
3. 运行api和admin

#### 使用说明

1. 默认api访问地址为127.0.0.1:8888/
2. 默认admin访问地址为127.0.0.1:8800/


