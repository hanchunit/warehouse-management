package com.gwlc.common.core;

import com.gwlc.common.base.BaseSerializableObject;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>warehouse-management-com.gwlc.common.base-Tree</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/16
 */
@Data
@NoArgsConstructor

public class Tree extends BaseSerializableObject {
    private static final long serialVersionUID = 3185872453544748691L;
    /**
     * 树的id
     */
    private Long id;
    /**
     * 父级树的id
     */
    private Long parentId;
    /**
     * 树名称
     */
    private String name;
    /**
     * 子树
     */
    private List<Tree> subtree;

    public Tree(Long treeId, Long parentId, String treeName, List<Tree> subtree) {
        this.id = treeId;
        this.parentId = parentId;
        this.name = treeName;
        this.subtree = subtree == null ? new ArrayList<>() : subtree;
    }


}
