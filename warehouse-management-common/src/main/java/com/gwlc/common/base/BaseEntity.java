package com.gwlc.common.base;


import lombok.Data;

import java.util.Date;

/**
 * <p>warehouse-management-com.gwlc.common-BaseEntity</p>
 * <p>Description: 基础实体类 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class BaseEntity extends BaseSerializableObject {
    private static final long serialVersionUID = 3009085090725317468L;
    /**
     * 创建者
     */
    protected String createBy;
    /**
     * 创建时间
     */
    protected Date createTime;
    /**
     * 更新者
     */
    protected String updateBy;
    /**
     * 更新时间
     */
    protected Date updateTime;
    /**
     * 备注
     */
    protected String remark;
    /**
     * 删除标记
     */
    protected Boolean deleted;
}
