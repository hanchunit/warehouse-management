package com.gwlc.common.base;

import java.io.Serializable;

/**
 * <p>team-management-system-com.gwlc.common.base-BaseSerializableObject</p>
 * <p>Description: 基础序列化类 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class BaseSerializableObject extends BaseObject implements Serializable {
    private static final long serialVersionUID = -2245298407443408387L;
}
