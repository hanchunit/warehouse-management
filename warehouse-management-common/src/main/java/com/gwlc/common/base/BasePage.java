package com.gwlc.common.base;

import lombok.Data;

import java.util.List;

/**
 * <p>warehouse-management-com.gwlc.common.base-BasePage</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/11
 */
@Data
public class BasePage<T, Q> extends BaseSerializableObject {
    private static final long serialVersionUID = -7349433309970411031L;
    /**
     * 页数
     */
    private Long page;
    /**
     * 数据条数
     */
    private Long limit;

    private Long lastLimit;


    /**
     * 数据
     */
    private List<T> data;

    private Q query;

    private int count;


    public void addLastLimit() {
        if (page != null && limit != null) {
            this.lastLimit = (page - 1) * limit;
        }
    }


}
