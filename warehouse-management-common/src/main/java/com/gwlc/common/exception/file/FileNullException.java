package com.gwlc.common.exception.file;

/**
 * <p>warehouse-management-com.gwlc.common.exception.file-FileNullException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class FileNullException extends FileException {
    private static final long serialVersionUID = -6138978029945896219L;

    /**
     * 文件为空
     */
    public FileNullException() {
        super(400, "文件为空");
    }
}
