package com.gwlc.common.exception.user;

import com.gwlc.common.enums.Exception.UserExceptionEnum;

/**
 * <p>warehouse-management-com.gwlc.common.exception.user-NotUserOrPasswordException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class NotUsernameOrPasswordException extends UserException {
    private static final long serialVersionUID = 3599324444320038029L;

    /**
     * 用户名或密码错误
     */
    public NotUsernameOrPasswordException() {
        super(UserExceptionEnum.NOT_USERNAME_OR_PASSWORD_EXCEPTION_ENUM);
    }
}
