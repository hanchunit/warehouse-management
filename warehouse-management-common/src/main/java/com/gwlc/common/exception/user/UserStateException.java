package com.gwlc.common.exception.user;

import com.gwlc.common.enums.Exception.UserExceptionEnum;

/**
 * <p>warehouse-management-com.gwlc.common.exception.user-UserStateException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class UserStateException extends UserException {
    private static final long serialVersionUID = -2431084083035638260L;

    /**
     * 用户状态异常
     */
    public UserStateException() {
        super(UserExceptionEnum.USER_STATE_EXCEPTION_ENUM);
    }
}
