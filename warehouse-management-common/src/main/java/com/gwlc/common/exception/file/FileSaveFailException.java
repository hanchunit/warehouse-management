package com.gwlc.common.exception.file;

/**
 * <p>warehouse-management-com.gwlc.common.exception.file-FileNotFoundException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class FileSaveFailException extends FileException {


    private static final long serialVersionUID = -4144508006206429133L;

    /**
     * 文件保存失败
     */
    public FileSaveFailException() {
        super(400, "文件保存失败！！！");
    }
}
