package com.gwlc.common.exception.user;

import com.gwlc.common.enums.Exception.UserExceptionEnum;

/**
 * <p>warehouse-management-com.gwlc.common.exception.user-TokenOutTimeException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/4
 */
public class TokenOutTimeException extends UserException {
    private static final long serialVersionUID = -4569422641733700227L;

    /**
     * 用户登录超时
     */
    public TokenOutTimeException() {
        super(UserExceptionEnum.TOKEN_OUT_TIME_EXCEPTION_ENUM);
    }
}
