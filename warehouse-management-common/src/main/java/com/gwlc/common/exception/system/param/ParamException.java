package com.gwlc.common.exception.system.param;

import com.gwlc.common.constant.system.ParamExceptionConstant;
import com.gwlc.common.exception.system.SystemException;

/**
 * <p>warehouse-management-com.gwlc.common.exception.system.parame-ParamException</p>
 * <p>Description: 参数异常 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/14
 */
public class ParamException extends SystemException {
    private static final long serialVersionUID = -7502303566609290624L;

    public ParamException(Integer status, String msg) {
        super(status, msg);
    }

    public ParamException(Integer status, String msg, Integer exist) {
        super(status, msg, exist);
    }

    /**
     * 参数异常，请检查输入参数
     */
    public ParamException() {
        super(400, ParamExceptionConstant.PARAM_EXCEPTION);
    }
}
