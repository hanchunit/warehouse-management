package com.gwlc.common.exception.file;

/**
 * <p>warehouse-management-com.gwlc.common.exception.file-FileTypeException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class FileTypeException extends FileException {


    private static final long serialVersionUID = 5577801909312473142L;

    /**
     * 文件类型不符合要求
     */
    public FileTypeException() {
        super(400, "上传文件类型不符");
    }
}
