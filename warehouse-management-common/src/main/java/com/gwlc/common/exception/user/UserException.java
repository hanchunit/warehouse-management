package com.gwlc.common.exception.user;


import com.gwlc.common.enums.Exception.UserExceptionEnum;
import com.gwlc.common.exception.base.BaseException;

import static com.gwlc.common.constant.system.user.UserExceptionConstant.USER_EXCEPTION;

/**
 * <p>warehouse-management-com.gwlc.common.exception.user-UserException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

public class UserException extends BaseException {
    private static final long serialVersionUID = -6139062394178464772L;

    public UserException(Integer status, String msg, Integer exist) {
        super(status, msg, exist);
    }

    public UserException(Integer status, String msg) {
        super(status, msg);
    }

    /**
     * 用户异常
     */
    public UserException() {
        super(401, USER_EXCEPTION);
    }

    public UserException(UserExceptionEnum userExceptionEnum) {
        super(userExceptionEnum.getCode(), userExceptionEnum.getMsg());
    }

}
