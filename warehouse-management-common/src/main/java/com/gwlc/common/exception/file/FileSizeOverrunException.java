package com.gwlc.common.exception.file;

/**
 * <p>warehouse-management-com.gwlc.common.exception.file-FileSizeOverrunException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class FileSizeOverrunException extends FileException {
    private static final long serialVersionUID = 2598774693246007217L;

    /**
     * 文件过大
     */
    public FileSizeOverrunException() {
        super(400, "文件大小超过限制，请重新上传");
    }
}
