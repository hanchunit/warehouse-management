package com.gwlc.common.exception.file;

/**
 * <p>warehouse-management-com.gwlc.common.exception.file-FileTypeException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class FileNotTypeException extends FileException {
    private static final long serialVersionUID = 5577801909312473142L;

    /**
     * 文件类型异常
     */
    public FileNotTypeException() {
        super(400, "未找到上传文件类型");
    }
}
