package com.gwlc.common.exception.system;

import com.gwlc.common.constant.system.SystemExceptionConstant;
import com.gwlc.common.exception.base.BaseException;

/**
 * <p>warehouse-management-com.gwlc.common.exception.system-SystemException</p>
 * <p>Description: 系统异常 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class SystemException extends BaseException {
    private static final long serialVersionUID = 5681589987429186753L;

    public SystemException(Integer status, String msg) {
        super(status, msg);
    }

    public SystemException(Integer status, String msg, Integer exist) {
        super(status, msg, exist);
    }

    /**
     * 系统异常
     */
    public SystemException() {
        super(400, SystemExceptionConstant.SYSTEM_EXCEPTION);
    }
}
