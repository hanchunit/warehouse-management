package com.gwlc.common.exception.user;

import com.gwlc.common.enums.Exception.UserExceptionEnum;

/**
 * <p>warehouse-management-com.gwlc.common.exception.user-UserStateException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class UserLockException extends UserException {

    private static final long serialVersionUID = -5048419817416410188L;

    /**
     * 用户已锁定
     */
    public UserLockException() {
        super(UserExceptionEnum.USER_LOCK_EXCEPTION_ENUM);
    }
}
