package com.gwlc.common.exception.user;

import com.gwlc.common.enums.Exception.UserExceptionEnum;

/**
 * <p>warehouse-management-com.gwlc.common.exception.user.user-NotTokenException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class NotTokenException extends UserException {
    private static final long serialVersionUID = 185112223367347338L;

    /**
     * 用户已在别处登录
     */
    public NotTokenException() {
        super(UserExceptionEnum.NOT_TOKEN_EXCEPTION_ENUM);
    }
}
