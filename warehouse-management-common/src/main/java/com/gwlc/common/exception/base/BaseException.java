package com.gwlc.common.exception.base;

import lombok.Data;
import org.springframework.http.HttpStatus;

import static com.gwlc.common.constant.system.SystemExceptionConstant.SYSTEM_EXCEPTION;

/**
 * <p>warehouse-management-com.gwlc.common.exception-BaseException</p>
 * <p>Description: 基础异常 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Data
public class BaseException extends RuntimeException {
    private static final long serialVersionUID = 3045672336970763775L;
    /**
     * 状态码
     */
    private Integer status;
    /**
     * 提示消息
     */
    private String msg;

    private Integer exist = null;

    public BaseException(Integer status, String msg) {
        this.msg = msg;
        this.status = status;
    }

    public BaseException(Integer status, String msg, Integer exist) {
        this.msg = msg;
        this.status = status;
        this.exist = exist;
    }

    public BaseException() {
        this.status = HttpStatus.BAD_REQUEST.value();
        this.msg = SYSTEM_EXCEPTION;
    }


}
