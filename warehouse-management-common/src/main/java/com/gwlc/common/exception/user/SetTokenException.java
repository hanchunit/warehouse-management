package com.gwlc.common.exception.user;

import com.gwlc.common.enums.Exception.UserExceptionEnum;

/**
 * <p>warehouse-management-com.gwlc.common.exception.user-SetTokenException</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class SetTokenException extends UserException {
    private static final long serialVersionUID = -7151716446632499268L;

    /**
     * 创建Token失败异常
     */
    public SetTokenException() {
        super(UserExceptionEnum.SET_TOKEN_EXCEPTION_ENUM);
    }
}
