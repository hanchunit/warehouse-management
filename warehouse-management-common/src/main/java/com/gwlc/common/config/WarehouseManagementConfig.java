package com.gwlc.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * <p>warehouse-management-com.gwlc.config-MorningBreezeImageConfig</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */

@Component
@ConfigurationProperties(prefix = "warehouse-management")
public class WarehouseManagementConfig {
    /**
     * url路径地址
     */
    private static String srcPath;
    /**
     * 实际路径地址
     */
    private static String savaPath;
    /**
     * 文件最大字节数
     */
    private static Long maxFileSize;

    private static Long outTime = 64800000L;

    private static Boolean verify = true;

    public static Boolean getVerify() {
        return verify;
    }

    public void setVerify(Boolean verify) {
        WarehouseManagementConfig.verify = verify;
    }

    public static String getSrcPath() {
        return srcPath;
    }

    public void setSrcPath(String srcPath) {
        WarehouseManagementConfig.srcPath = srcPath;
    }

    public static String getSavaPath() {
        return savaPath;
    }

    public void setSavaPath(String savaPath) {
        WarehouseManagementConfig.savaPath = savaPath;
    }

    public static Long getMaxFileSize() {
        return maxFileSize;
    }

    public void setMaxFileSize(Long maxFileSize) {
        WarehouseManagementConfig.maxFileSize = maxFileSize;
    }

    public static Long getOutTime() {
        return outTime;
    }

    public void setOutTime(Long outTime) {
        WarehouseManagementConfig.outTime = outTime;
    }
}
