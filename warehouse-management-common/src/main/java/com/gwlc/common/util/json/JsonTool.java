package com.gwlc.common.util.json;

import com.alibaba.fastjson.JSON;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * <p>warehouse-management-com.gwlc.common.util.json-JsonTool</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class JsonTool {
    public static <T> List<Map<String, T>> toListMap(String json) {
        List<T> list = (List<T>) JSON.parseArray(json);

        List<Map<String, T>> listw = new ArrayList<>();
        for (Object object : list) {
            //取出list里面的值转为map
            Map<String, T> ret = (Map<String, T>) object;

            listw.add(ret);
        }
        return listw;

    }
}
