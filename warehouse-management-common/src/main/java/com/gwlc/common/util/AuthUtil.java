package com.gwlc.common.util;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>warehouse-management-com.gwlc.common.util-AuthUtil</p>
 * <p>Description: session工具类</p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/8
 */
public class AuthUtil {
    /**
     * 根据token拿去用户id
     *
     * @param request
     * @return
     * @throws Exception
     */
    public static Long getUserId(HttpServletRequest request) throws Exception {
        return Long.valueOf((Integer) getClientLoginInfo(request).get("userId"));

    }

    /**
     * 这个类方法是面向手机客户端的，从而实现的Token机制。实现请见上述文章：
     */
    private static Map<String, Object> getClientLoginInfo(HttpServletRequest request) throws Exception {
        Map<String, Object> r = new HashMap<>();
        String sessionId = request.getHeader("sessionId");
        if (sessionId != null) {
            r = decodeSession(sessionId);
            return r;
        }
        throw new Exception("session解析错误");
    }

    /**
     * session解密
     */
    public static Map<String, Object> decodeSession(String sessionId) {
        try {
            return verifyJavaWebToken(sessionId);
        } catch (Exception e) {
            System.err.println("");
            return null;
        }
    }

    private static Map<String, Object> verifyJavaWebToken(String sessionId) {
        return null;
    }
}

