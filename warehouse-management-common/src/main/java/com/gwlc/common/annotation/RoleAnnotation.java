package com.gwlc.common.annotation;

import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.lang.annotation.*;

/**
 * <p>warehouse-management-com.gwlc.system.common.annotation-RoleAnnotation</p>
 * <p>Description: 角色验证注解 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/28
 */

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@EnableAspectJAutoProxy(proxyTargetClass = true)
public @interface RoleAnnotation {
    /**
     * 角色标签数组(为空则值验证是否登录)
     *
     * @return 角色标签
     */
    String[] value() default {};


}