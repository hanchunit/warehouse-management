package com.gwlc.common.annotation;

import org.springframework.context.annotation.EnableAspectJAutoProxy;

import java.lang.annotation.*;

/**
 * <p>warehouse-management-com.dawn.common.annotation-PermissionAnnotation</p>
 * <p>Description: 登录及权限验证注解 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@EnableAspectJAutoProxy(proxyTargetClass = true)
public @interface PermissionAnnotation {
    /**
     * 权限标签数组(为空则值验证是否登录)
     *
     * @return 权限标签
     */
    String[] value() default {};


}
