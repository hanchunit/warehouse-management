package com.gwlc.common.enums.system.user;

import com.gwlc.common.exception.user.UserDisableException;
import com.gwlc.common.exception.user.UserException;
import com.gwlc.common.exception.user.UserLockException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import static com.gwlc.common.constant.system.user.UserStateConstant.*;

/**
 * <p>warehouse-management-com.gwlc.common.enums.system.user-UserStatusEnum</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/3
 */

@AllArgsConstructor
@Getter
public enum UserStatusEnum {
    /**
     * 用户状态异常
     */
    ABNORMALITY(USER_STATE_ABNORMALITY, "用户状态异常"),
    /**
     * 用户状态正常
     */
    NORMAL(USER_STATE_NORMAL, "用户正常"),
    /**
     * 用户已锁定
     */
    LOCK(USER_STATE_LOCK, "用户已锁定"),
    /**
     * 用户已停用
     */
    DISABLE(USER_STATE_DISABLE, "用户已停用");

    private Integer value;
    private String describe;

    /**
     * 验证用户状态
     *
     * @param userStatus 用户状态
     */
    public static void verifyUserStatus(Integer userStatus) {
        UserStatusEnum userStatusEnum = valueOf(userStatus);
        if (userStatusEnum == null) {
            throw new UserException();
        }
        verifyUserStatus(userStatusEnum);

    }

    public static UserStatusEnum valueOf(int value) {
        for (UserStatusEnum userStateEnum : values()) {
            if (userStateEnum.value.equals(value)) {
                return userStateEnum;
            }
        }
        return null;
    }

    /**
     * 验证用户状态
     *
     * @param userStatusEnum 用户状态枚举
     */
    public static void verifyUserStatus(UserStatusEnum userStatusEnum) {
        switch (userStatusEnum) {
            case NORMAL:
                return;
            case DISABLE:
                throw new UserDisableException();
            case LOCK:
                throw new UserLockException();
            case ABNORMALITY:
            default:
                throw new UserException();
        }
    }

    @Override
    public String toString() {
        return getDescribe();
    }

}
