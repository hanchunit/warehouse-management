package com.gwlc.common.enums.Exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

import static com.gwlc.common.constant.system.user.UserExceptionConstant.*;

/**
 * <p>warehouse-management-com.gwlc.common.enums.Exception-UserExceptionEnum</p>
 * <p>Description: 用户异常枚举 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/3
 */

@AllArgsConstructor
@Getter
public enum UserExceptionEnum {
    /**
     * 用户未登录异常
     */
    NOT_LOGIN_EXCEPTION_ENUM(401, NOT_LOGIN_EXCEPTION),


    /**
     * 用户权限不足异常
     */
    NOT_PERMISSION_EXCEPTION_ENUM(403, NOT_PERMISSION_EXCEPTION),

    /**
     * 用户令牌失效异常
     */
    NOT_TOKEN_EXCEPTION_ENUM(401, NOT_TOKEN_EXCEPTION),

    /**
     * 用户名不存在异常
     */
    NOT_USERNAME_EXCEPTION_ENUM(400, NOT_USERNAME_EXCEPTION),

    /**
     * 用户名或密码错误异常
     */
    NOT_USERNAME_OR_PASSWORD_EXCEPTION_ENUM(400, NOT_USERNAME_OR_PASSWORD_EXCEPTION),

    /**
     * 设置用户令牌失败异常
     */
    SET_TOKEN_EXCEPTION_ENUM(400, SET_TOKEN_EXCEPTION),
    /**
     * 用户令牌失效异常
     */
    TOKEN_OUT_TIME_EXCEPTION_ENUM(400, TOKEN_OUT_TIME_EXCEPTION),
    /**
     * 用户已停用异常
     */
    USER_DISABLE_EXCEPTION_ENUM(400, USER_DISABLE_EXCEPTION),
    /**
     * 用户异常
     */
    USER_EXCEPTION_ENUM(401, USER_EXCEPTION),

    /**
     * 用户已锁定异常
     */
    USER_LOCK_EXCEPTION_ENUM(401, USER_LOCK_EXCEPTION),
    /**
     * 用户状态异常
     */

    USER_STATE_EXCEPTION_ENUM(400, USER_STATE_EXCEPTION);
    /**
     * 状态码
     */
    int code;
    /**
     * 状态消息
     */
    String msg;
}
