package com.gwlc.common.enums.system.user;

/**
 * <p>team-management-com.gwlc.common.enums-UserEnum</p>
 * <p>Description: 用户类型 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/3
 */
public enum UserEnum {
    USER_STATE_NORMAL,
    USER_STATE_LOCK,
    USER_STATE_DISABLE,
    USER_TYPE_ROOT,
    USER_TYPE_ADMIN,
    USER_TYPE_COMMON;
    private Integer constant;
    private String value;
}
