package com.gwlc.common.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

/**
 * <p>team-management-system-com.gwlc.common.exception-ExceptionStatusEnum</p>
 * <p>Description: 异常状态码 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/7/30
 */
@ToString
@AllArgsConstructor
@Getter
public enum ExceptionStatusEnum {
    //成功状态码 00 通用  000成功
    SUCCESS("00000", "成功"),
    //失败状态码
    //10 参数
    PARAMS_IS_NULL("10001", "参数为空"),
    PARAMS_NOT_COMPLETE("10002", "参数不全"),
    PARAMS_TYPE_ERROR("10003", "参数类型错误"),
    PARAMS_IS_INVALID("10004", "参数无效"),
    //用户错误
    USER_NOT_EXIST("20001", "用户不存在"),
    USER_NOT_LOGGED_IN("20002", "用户未登录"),
    USER_ACCOUNT_ERROR("20003", "用户名或密码错误"),
    USER_ACCOUNT_FORBIDDEN("20005", "用户账户已被禁用"),
    USER_HAS_EXIST("20005", "用户已存在"),
    // 业务错误
    BUSINESS_ERROR("30001", "系统业务错误"),
    SYSTEM_INNER_ERROR("40001", "系统内部错误"),

    // 数据错误
    DATA_NOT_FOUND("50001", "数据未找到"),
    DATA_IS_WRONG("50002", "数据有误"),
    DATA_ALREADY_EXISTED("50003", "数据已存在"),

    ;
    /**
     * 状态码 前两位为分类标志 后三位为状态码
     */
    private String status;
    /**
     * 信息
     */
    private String msg;

}
