package com.gwlc.common.enums.system.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

import static com.gwlc.common.constant.system.user.UserTypeConstant.*;

/**
 * <p>warehouse-management-com.gwlc.common.enums.system.user-UserTypeEnum</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/3
 */
@ToString
@AllArgsConstructor
@Getter
public enum UserTypeEnum {
    /**
     * 根管理员
     */
    ROOT(USER_TYPE_ROOT, "根管理员"),
    /**
     * 普通管理员
     */
    ADMIN(USER_TYPE_ADMIN, "普通管理员"),
    /**
     * 普通用户
     */
    COMMON(USER_TYPE_COMMON, "普通用户");
    private Integer value;
    private String describe;

    public static UserTypeEnum valueOf(Integer value) {
        for (UserTypeEnum userTypeEnum : values()) {
            if (userTypeEnum.value.equals(value)) {
                return userTypeEnum;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return getDescribe();
    }
}
