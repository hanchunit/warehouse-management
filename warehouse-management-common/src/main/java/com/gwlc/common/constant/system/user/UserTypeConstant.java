package com.gwlc.common.constant.system.user;

/**
 * <p>warehouse-management-com.gwlc.common.constant.system.user-UserConstant</p>
 * <p>Description: 用户常量 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class UserTypeConstant {
    /**
     * 用户类型:超级管理员
     */
    public static int USER_TYPE_ROOT = 0;
    /**
     * 用户类型:普通管理员
     */
    public static int USER_TYPE_ADMIN = 1;
    /**
     * 用户类型:普通用户
     */
    public static int USER_TYPE_COMMON = 2;
}
