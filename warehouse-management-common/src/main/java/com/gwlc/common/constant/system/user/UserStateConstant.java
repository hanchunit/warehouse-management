package com.gwlc.common.constant.system.user;

/**
 * <p>warehouse-management-com.gwlc.common.constant.system.user-UserStateConstant</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/3
 */
public class UserStateConstant {
    public static int USER_STATE_ABNORMALITY = -1;
    /**
     * 用户状态:正常
     */
    public static int USER_STATE_NORMAL = 0;
    /**
     * 用户状态:锁定
     */
    public static int USER_STATE_LOCK = 1;
    /**
     * 用户状态:停用
     */
    public static int USER_STATE_DISABLE = 2;

}
