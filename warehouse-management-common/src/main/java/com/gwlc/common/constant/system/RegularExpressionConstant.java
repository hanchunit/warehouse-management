package com.gwlc.common.constant.system;

/**
 * <p>warehouse-management-com.gwlc.common.constant.system-RegularExpressionConstant</p>
 * <p>Description: 正则表达式常量 </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/13
 */
public class RegularExpressionConstant {
    /**
     * 帐号是否合法(字母开头，允许4-16字节，允许字母数字下划线)
     */
    public static String USER_NAME_VERIFY = "^[a-zA-Z][a-zA-Z0-9_]{3,15}$";
    /**
     * 密码(以字母开头，长度在6~18之间，只能包含字母、数字和下划线)
     */
    public static String USER_PASSWORD_VERIFY = "^[a-zA-Z]\\w{5,17}$";


}
