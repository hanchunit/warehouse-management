package com.gwlc.common.constant.system;

/**
 * <p>warehouse-management-com.gwlc.common.constant.system.SystemExceptionConstant-SystemExceptionConstant</p>
 * <p>Description:  系统常量</p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */


public class SystemExceptionConstant {
    /**
     * 系统异常，请联系管理员
     */
    public static String SYSTEM_EXCEPTION = "系统异常，请联系管理员";


}
