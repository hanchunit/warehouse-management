package com.gwlc.common.constant.system;

/**
 * <p>warehouse-management-com.gwlc.common.constant.system-ParamExceptionConstant</p>
 * <p>Description:  参数异常常量</p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/8/14
 */
public class ParamExceptionConstant {

    /**
     * 参数异常，请检查输入参数
     */
    public static String PARAM_EXCEPTION = "参数异常，请检查输入参数";
    /**
     * 参数类型异常，请检查输入参数
     */
    public static String PARAM_TYPE_EXCEPTION = "参数类型异常，请检查输入参数";
    /**
     * 参数溢出异常，请检查输入参数
     */
    public static String PARAM_OVERFLOW_EXCEPTION = "参数溢出异常，请检查输入参数";

    /**
     * 参数数量异常，请检查输入参数
     */
    public static String PARAM_NUMBER_EXCEPTION = "参数数量异常，请检查输入参数";
    /**
     * 参数大小异常，请检查输入参数
     */
    public static String PARAM_SIZE_EXCEPTION = "参数大小异常，请检查输入参数";


}
