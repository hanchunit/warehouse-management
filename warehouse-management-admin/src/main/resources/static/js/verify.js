/**
 * 根地址
 * @type {string}
 */
const BASE_URL = "https://t4060916n0.goho.co";
/**
 * 登录页面地址
 * @type {string}
 */
const LOGIN_URL = "/login";
/**
 * api地址
 * @type {string}
 */
const API_URL = "http://127.0.0.1:8888";

const INDEX_URL = "/index";

let loginFlag = false;


/**
 * post方式
 * @param url
 * @param data
 * @param async
 * @param dataType
 * @param contentType
 * @return {*}
 */
function postAjax(url, data = "", async = false, dataType = "json", contentType = "application/json") {
    let result;
    $.ajax({
        type: "post",
        url: url,
        data: data,
        async: async,
        dataType: dataType,
        contentType: contentType,
        headers: {"token": getCookie("token") ? getCookie("token") : get("token")},
        success: function (resultData) {
            result = resultData;
        },
        error: function () {
            layer.alert("网络错误", {
                time: 5 * 1000
                , success: function (layero, index) {
                    let timeNum = this.time / 1000, setText = function (start) {
                        layer.title((start ? timeNum : --timeNum) + ' 秒后关闭', index);
                    };
                    setText(!0);
                    this.timer = setInterval(setText, 1000);
                    if (timeNum <= 0) clearInterval(this.timer);
                }
                , end: function () {
                    clearInterval(this.timer);
                }
            });
        }
    });

    return result;
}

/**
 * get方式
 * @param url
 * @param data
 * @param async
 * @return {*}
 */
function getAjax(url, data = "", async = false) {
    let result;
    $.ajax({
        type: "get",
        url: url,
        data: data,
        async: async,
        headers: {"token": getCookie("token") ? getCookie("token") : get("token")},
        success: function (resultData) {
            result = resultData;
        },
        error: function () {
            layer.alert("网络错误", {
                time: 5 * 1000
                , success: function (layero, index) {
                    let timeNum = this.time / 1000, setText = function (start) {
                        layer.title((start ? timeNum : --timeNum) + ' 秒后关闭', index);
                    };
                    setText(!0);
                    this.timer = setInterval(setText, 1000);
                    if (timeNum <= 0) clearInterval(this.timer);
                }
                , end: function () {
                    clearInterval(this.timer);
                }
            });
        }
    });
    return result;
}

/**
 * 表格重载
 * @param elem
 * @param method
 * @param url
 * @param limits
 * @param limit
 * @param title
 * @param cols
 * @param id
 */
function tableRender(elem, method, url, limits = [5, 10, 15], limit = 5, title, cols = [], id) {

    layui.use('table', function () {
        const table = layui.table;
        table.render({
            elem: elem,
            method: method,
            url: url,//数据接口
            headers: {"token": getCookie("token")},
            parseData: function (res) { //res 即为原始返回的数据
                return {
                    "code": res.meta.status === 200 ? 0 : 1, //解析接口状态
                    "msg": res.meta.msg, //解析提示文本
                    "count": res.data.count, //解析数据长度
                    "data": res.data.data //解析数据列表
                };
            },

            limits: limits,
            limit: limit,
            contentType: 'application/json',
            title: title,
            toolbar: '#toolbarDemo', //开启头部工具栏，并为其绑定左侧模板
            page: { //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
                layout: ['limit', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
                //,curr: 5 //设定初始在第 5 页
                , groups: 1 //只显示 1 个连续页码
                , first: false //不显示首页
                , last: false //不显示尾页

            },
            cols: cols,
            id: id,
            escape: true,
            height: 'full-125'

        });
    });
}


function tableEdit(elem, url, id) {

    layui.use('table', function () {
        const table = layui.table;
        table.on(elem, function (obj) {
            const value = obj.value //得到修改后的值
                , data = obj.data //得到所在行所有键值
                , field = obj.field; //得到字段
            let editData = {}
            editData[field] = value;
            editData[id] = data[id];
            const updateFlag = postAjax(url, JSON.stringify(editData));
            if (updateFlag) {
                layer.msg(updateFlag.meta.msg);


            }

        });

    });
}

/**
 * 删除
 * @param elem
 * @param url
 * @param id
 */
function tableDelete(elem, url, id) {
    layui.use('table', function () {
        const table = layui.table;
        table.on(elem, function (obj) {
            const data = obj.data;
            //console.log(obj)

            switch (obj.event) {
                case 'del':
                    layer.confirm('真的删除行么', function (index) {
                        const deleteFlag = getAjax(url + data[id]);
                        if (deleteFlag && deleteFlag.meta.status === 200) {
                            obj.del();
                            layer.close(index);
                        } else {
                            layer.close(index);
                            layer.msg(deleteFlag.meta.msg);
                        }
                    });
                    break;
                case 'detail':

                default:
                    break;
            }


        });
    });
}

function tableAdd() {

}

/**
 * 验证是否登录
 * @return {boolean} 是否登录
 */
function verifyLogin() {

    const token = get("token");
    if (token == null || token === "") {

        layer.alert("用户未登录, 请登录", {
            time: 5 * 1000
            , success: function (layero, index) {
                let timeNum = this.time / 1000, setText = function (start) {
                    layer.title((start ? timeNum : --timeNum) + ' 秒后关闭', index);
                };
                setText(!0);
                this.timer = setInterval(setText, 1000);
                if (timeNum <= 0) {
                    clearInterval(this.timer);
                    jumpUrl(LOGIN_URL)
                }
            }
            , end: function () {
                clearInterval(this.timer);
                jumpUrl(LOGIN_URL)
            }
        });

        return false;
    } else {


        return true;

    }
}

function jumpUrl(url) {

    // $.ajax({
    //     type: "get",
    //     url: BASE_URL + url,
    //     async: false,
    //     beforeSend: function (xhr) {
    //         xhr.withCredentials = true;
    //     },
    //     crossDomain: true,
    //     headers: {"token": getCookie("token")},
    //
    // });
    window.location.replace(BASE_URL + url);
}

function set(type, value) {
    localStorage.setItem(type, value);
}

function get(type) {
    return localStorage.getItem(type);
}

/**
 * 这个cookie会覆盖已经建立过的同名、同path的cookie（如果这个cookie存在）。
 * @param name 键名
 * @param value 值
 * @param hours hours为空字符串时,cookie的生存期至浏览器会话结束。hours为数字0时,建立的是一个失效的cookie
 */

function setCookie(name, value, hours) {
    const d = new Date();
    d.setTime(d.getTime() + (hours * 24 * 60 * 60 * 1000));
    const expires = "expires=" + d.toUTCString();
    document.cookie = name + "=" + value + "; " + expires;
}

/**
 * 根据cookie名获取值
 * @param name cookie名
 * @return {string} 值
 */
function getCookie(name) {
    name = name + "=";
    const ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        const c = ca[i].trim();
        if (c.indexOf(name) === 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}