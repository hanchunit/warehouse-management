package com.gwlc.admin.config;


import com.gwlc.common.exception.base.BaseException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>warehouse-management-com.dawn.config-MyResponseBodyAdvice </p>
 * <p>Description:  全局异常捕获类(作用是异常信息统一格式输出) </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Component
@RestControllerAdvice

public class MyResponseBodyAdvice {

    public MyResponseBodyAdvice() {
    }


    /**
     * 全局自定义异常TestException捕获类
     *
     * @param baseException 基础异常
     * @return 异常消息
     */
    @ExceptionHandler(value = BaseException.class)
    public ModelAndView userExceptionHandler(BaseException baseException) {

        Map<String, Object> map = new HashMap<>(16);
        map.put("status", baseException.getStatus());
        map.put("msg", baseException.getMsg());
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("meta", map);
        resultMap.put("data", null);
        return new ModelAndView("error", resultMap);
    }


}