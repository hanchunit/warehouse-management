package com.gwlc.admin.config;

import com.gwlc.common.config.WarehouseManagementConfig;
import com.gwlc.common.constant.system.user.UserStateConstant;
import com.gwlc.common.exception.base.BaseException;
import com.gwlc.common.exception.system.SystemException;
import com.gwlc.common.exception.user.NotLoginException;
import com.gwlc.common.exception.user.NotTokenException;
import com.gwlc.common.exception.user.TokenOutTimeException;
import com.gwlc.common.exception.user.UserStateException;
import com.gwlc.common.util.TokenTool;
import com.gwlc.system.domain.entity.UserDO;
import com.gwlc.system.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>warehouse-management-com.dawn.config-LoginHandlerInterceptor</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@Component
//@Configuration
public class LoginHandlerInterceptor implements HandlerInterceptor {

    Boolean verify = WarehouseManagementConfig.getVerify();
    @Autowired
    private IUserService userService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws ServletException, IOException {


        if (verify != null && !verify) {
            return true;
        }
        System.out.println("**************访问校验*****************");
        String token = getToken(request);
        //request.getHeader("token");

        System.out.println(token);


        try {

            Long outTime = null;
            try {
                if (token == null || "".equals(token)) {
                    throw new NotLoginException();
                }
                outTime = (Long) (TokenTool.decodeTokenMap(token)).get("outTime");
            } catch (Exception e) {
                throw new NotLoginException();
            }
            long newTime = System.currentTimeMillis();
            if (outTime == null || outTime <= newTime) {
                throw new TokenOutTimeException();
            }

            UserDO user = userService.getUserByToken(token);
            if (user == null || user.getUserId() == null || user.getUserId() <= 0 || user.getUserToken().isEmpty()) {
                throw new NotTokenException();
            }
            if (!user.getUserStatus().equals(UserStateConstant.USER_STATE_NORMAL)) {
                throw new UserStateException();
            }


        } catch (BaseException e) {
            request.setAttribute("filter.error", e);
            System.out.println("将异常分发到/baseException/errors控制器");
            System.out.println(e.toString());
            //将异常分发到/baseException/errors控制器
            request.getRequestDispatcher("/base_exception/errors").forward(request, response);
        } catch (Exception e) {
            request.setAttribute("filter.error", new SystemException());
            System.out.println("将异常分发到/baseException/errors控制器");
            System.out.println(e.toString());
            //将异常分发到/baseException/errors控制器
            request.getRequestDispatcher("/base_exception/errors").forward(request, response);
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
        HandlerInterceptor.super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        HandlerInterceptor.super.afterCompletion(request, response, handler, ex);
    }

    /**
     * 获取cookie中的token
     *
     * @param request 请求
     * @return 结果
     */
    private String getToken(HttpServletRequest request) {
        //获取本地的cookies
        String token = null;
        Cookie[] cookies = request.getCookies();
        if (null != cookies) {
            for (Cookie cookie : cookies) {
                if ("token".equals(cookie.getName())) {
                    token = cookie.getValue();
                }
            }
        }
        return token;

    }

}
