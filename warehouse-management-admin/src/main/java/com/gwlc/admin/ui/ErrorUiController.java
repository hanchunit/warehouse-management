package com.gwlc.admin.ui;

import com.gwlc.common.exception.base.BaseException;
import com.gwlc.common.exception.system.SystemException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>warehouse-management-com.dawn.controller-ErrorUiController</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date
 */
@RestController
@RequestMapping("/base_exception")
public class ErrorUiController {


    @RequestMapping("/errors")
    public ModelAndView error(HttpServletRequest request) throws Exception {
        BaseException error = (BaseException) request.getAttribute("filter.error");
        if (error == null) {
            error = new SystemException();
        }
        Map<String, Object> map = new HashMap<>(16);
        map.put("status", error.getStatus());
        map.put("msg", error.getMsg());
        Map<String, Object> resultMap = new HashMap<>();
        resultMap.put("meta", map);
        resultMap.put("data", null);

        System.out.println("异常消息:" + resultMap);
        return new ModelAndView("error", resultMap);

    }
}
