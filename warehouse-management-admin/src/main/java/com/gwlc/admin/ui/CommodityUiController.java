package com.gwlc.admin.ui;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>warehouse-management-com.gwlc.admin.ui-CommodityUiController</p>
 * <p>Description:  </p>
 * <p>Copyright: Copyright (c) 2021</p>
 * <p>Company: hanchunit@163.com</p>
 *
 * @author hanchun
 * @version 1.0
 * @date 2021/9/10
 */
@Controller
@RequestMapping("/commodity")
public class CommodityUiController {
    public static String PATH = "/commodity";

    @GetMapping("/list")
    public String list() {
        return PATH + "/list";
    }

    @GetMapping("/add")
    public String add() {
        return PATH + "/add";
    }

    @GetMapping("/edit")
    public String edit() {


        return PATH + "/edit";
    }


}
